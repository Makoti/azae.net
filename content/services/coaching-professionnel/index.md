---
title: Coaching professionnel
menu:
  left:
    parent: Services
  footer:
    parent: Services
description: >
    Aider les coachés, à dépasser leurs obstacles, croîtrent et gagner en efficacité, car nous croyons en l'émergence des solutions par ceux qui les possèdent, c'est-à-dire vous.
show_on_index:
  order: 5
  icon: fa-heart
  color: color-scarlet-red
  summary: >
    Aider les coachés, à dépasser leurs obstacles, croîtrent et gagner en efficacité.

---

## Qui est le coach professionnel ?

L'EMCC (European Mentoring and Coaching Council, une des fédérations professionnelles de coaching), donne [cette définition](https://www.emccfrance.org/types-daccompagnement/) :

> Le coach est un professionnel de l'accompagnement qui a suivi une formation spécifique au coaching, formation qui respecte un référentiel de compétences précis, défini en collaboration avec les fédérations professionnelles. Le coach respecte le Code de Déontologie de la fédération professionnelle à laquelle il est rattaché. Son rôle est d'aider le coaché, avec des processus d'accompagnement adaptés, à bâtir ses propres solutions. Contrairement au consultant, son rôle principal n'est pas d'apporter du contenu et il ne connaît pas forcément les aspects techniques du métier du coaché. Il est là pour mettre en œuvre des processus qui aident le coaché, à dépasser ses obstacles, croître et gagner en efficacité.

Notre pratique est essentiellement liée au contexte dans lequel nous intervenons, elle sera toujours déroulée dans la confiance, la transparence et le consentement des différentes parties prenantes.

## Comment se déroule nos coachings professionnels ?

Le coaching professionnel commence par un entretien de rencontre durant lequel le coach présentera sa pratique et son code de déontologie. Il pourra également présenter son cadre de référence et sa pratique. Ce premier entretien est gratuit et n'engage ni le coach, ni le client dans une relation de coaching.
Suite à cet entretien, si le coaché et le coach souhaitent travailler ensemble, un contrat de coaching sera établi. Le coach s'engage alors à accompagner le coaché vers son objectif dans un accompagnement bref (maximum une douzaine de sessions).
L'accompagnement de coaching se clotûre par une session de bilan.

## Notre spécialiste Julie Quillé
### Qui est-elle ?

Suivant des formations dans l'accompagnement depuis près de 15 ans, je me suis intéressée à différents aspects des métiers d'accompagnement des personnes et des équipes : psychologie, accompagnement émotionnel et corporel, communication, systémique, gestion de conflit, définition de cadre commun etc.
Coach professionnelle certifiée, je mobilise aujourd'hui ces différentes compétences pour assurer la qualité humaniste de mon approche.
J'accompagne équipes et managers, en individuel ou en groupe dans une démarche de coaching professionnel qui vient en complément de mon rôle de consultante agile en entreprise.

Membre de l'EMCC je souscris à [son code de déontologie](https://www.emccfrance.org/wp-content/uploads/20170418_FR_WEB_livret.pdf
) et invite chacun de mes clients à prendre connaissance du [code de déontologie d'Azaé](https://azae.gitlab.io/secretariat/code-de-deontologie/code-de-deontologie.pdf), qui vient en complément.

### Quelle est sa spécificité en tant que coach professionnel ?

En tant que coach, j'accompagne plus particulièrement les managers qui souhaitent s'orienter vers un nouveau type de management. Je travaille particulièrement sur la définition des frontières et les nouveaux types de management pour encourager l'autonomie, la responsabilité et l'agilité des équipes, des leaders et des structures.

## Comment nous contacter ?

Si vous souhaitez davantage d'informations sur nos accompagnements,  vous pouvez nous contacter à partir de l'onglet [Contact](https://azae.net/contact/) ou directement Julie Quillé au 06 35 58 93 81.
