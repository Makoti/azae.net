---
title: Coaching Software Craftsmanship
menu:
  left:
    parent: Services
  footer:
    parent: Services
description: >
    L'excellence opérationnelle ou produire des logiciels qui apportent de la valeur. 
    Notre objectif est de rehausser significativement le niveau de ce qui est attendu de la part de la profession de développeur logiciel, que ce soit en termes de fiabilité, de qualité et de transparence.
show_on_index:
  order: 3
  icon: fa-diamond
  color: color-chameleon
  summary: >
    L'excellence opérationnelle ou produire des logiciels qui apportent de la valeur.

---
# Notre promesse

Vous aider à améliorer le flux de production de vos équipes pour livrer :
* plus **vite**,
* plus de **valeur**, 
* plus **sereinement**.

# Notre vision

Imaginez : un reponsable produit à une idée géniale pour répondre à un problème client, l'équipe de développement promet de livrer la fonctionalité en 1 sprint, ça en prend 3 pour la mettre en production. Mais elle apporte des regressions sur d'autres fonctionalités, ils mettent un sprint pour les régler. Lorsque que c'est stabilisé le client se rend compte que son problème n'est pas résolu.

Au lieu de cela :  Un reponsable produit à une idée géniale pour répondre à un problème client, l'équipe et le client collaborent, co-construisent, et expérimentent différentes stratégies avant même la fin du premier sprint et réglent une problèmatique client en production en 1 sprint.

Vous avez engagé des personnes intelligentes dans votre entreprise, vos équipes sont compétentes, la lenteur, la dette technique et l'inadéquations des solutions logicielles ne sont que les conséquence d'un problème systémique que nous vous aiderons à résoudre.

<!--
# Notre solution

Les objectifs que nous visons sont les suivants :
 * Construire et fédérer l'équipe de développement
 * Accompagner et faire progresser l'équipe vers l'excellence.
 * Garantir la abilité et la qualité des applications produites
Pour ce faire, nous nous appuierons sur un ensemble d'ateliers. Notre stratégie pour atteindre ces
objectifs consiste à vous conseiller et vous accompagner sur un ensemble d'éléments :
 Mise en place des chaines d'intégration et de déploiement continues multi-plateformes
 Mise en place des tests automatisés.
 Mise en place d'une véritable stratégie de test adaptée à vos besoins et à vos contraintes.
 Mise en place d'une documentation vivante (mise en relation des spécications client avec
le code source)
 Mise en place d'une démarche craft (recherche de l'excellence et amélioration technique en
continue).
 Identication, formation et accompagnement des personnes capables de remplir le rôle de
scrum master.
Durant l'ensemble de notre intervention, nous nous appuierons sur notre expérience et notre exper-
tise pour vous apporter des conseils adaptés à vos contraintes. Notons par exemple la contrainte
de respecter autant que possible les standards et les outils de RATP Smart Systems.
Nous attirons votre attention sur le point suivant : d'après notre expérience, nous recommandons
que les managers se libèrent du temps pour que nous puissions les accompagner, car leur impact
sur les équipes de développement est important.
Notre objectif avec ce dispositif est bien de rehausser signicativement le niveau de ce qui est
attendu de la part de la profession de développeur logiciel, que ce soit en termes de abilité, de
qualité et de transparence.
Voici quelques exemples d'atelier qui seront majoritairement construits pour vous et autour de
votre code et de vos problématiques :
 mob-programming.
 binômage avec chacun des membres de l'équipe.
 katas.
 coding dojo.
La facturation se fera chaque n de mois en fonction du nombre réel de jours consommés. La
construction et la préparation des ateliers se feront majoritairement à distance loin de toutes in-
terruptions.
Le rôle du coach dans ce genre d'intervention se décompose en 3 axes :
 Observer les pratiques de l'équipe pour identier les points forts sur lesquels il faudra s'ap-
puyer ainsi que les points faibles qu'il sera nécessaire de travailler.
 Faire prendre conscience aux équipes et aux managers de ce qui pourrait être amélioré (étape
souvent longue et compliquée)
 Former puis accompagner dans le temps les changements.


# Success Stories

# 


Messages clés :
    - les défauts des équipes et du code actuel

        - dette technique

           - manque d'entrainement
           - 
    - notre posture

        - coach : 

        - pédagogue / formateur : capable d'utiliser nos formations sur étagère comme de construire des formations sur mesure.

        - mentor : fort d'une très grande expérience en développement logiciel ... bla bla

    - comment fonctionne le coaching ?

        - on construit l'intervention avec vous en fonction de vos besoins

    - bénéfice pour l'équipe et l'entreprise
    - belles histoires

        


formations à lier

     - toute la catégorie culture du développement


Talks atelier que l'on fait : 
    - légo TDD
    - micro service
    - BDD

-->
