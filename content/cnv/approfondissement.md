---
title: Exploration de la Communication NonViolente
permalink: /approfondissement/
description: |
    Approfondissement de la CNV dans la philosophie et la pratique.

---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
<div class="col-md-12">
<header class="proposition">
 <h2>La proposition</h2>
 <div class="girafe">
  <ul class="participation">
   <li><a class="bouton sabonner" href='https://5c5fda8f.sibforms.com/serve/MUIEAHHFl0Svw8_xI01j4j2hkRai6otFy9odx4CIx4ef2oJwauaDteoH4vVBpbe-BQy8uBnD6VqaVLjTJasb94qxqyHrE8vDTcKGa_D2-OyCIpG5tfgSPPUskjp6xtOs4k384_jd1QptB-gFaWWfN9oAqNNOkpOM7-lxx7jEPVUz3f1yDFSAMqzBlzn5o_KKE_zX3Dh5jZxD8aeZ'>S'ABONNER</a></li>
   <li><a class="bouton soutenir" style="margin-top: 1em;" href='https://lydia-app.com/collect/71818-cnv-julie-diane/fr'>SOUTENIR</a></li>
  </ul>
  <ul>
   <li>Aller plus en profondeur dans la CNV</li>
   <li>4 heures</li>
   <li>un groupe de 4 à 15 personnes passionnantes</li>
   <li>2 animatrices passionnées</li>
   <li>de l'échange, du lien, de l'expérimentation et des apprentissages</li>
  </ul>
 </div>
</header>
<hr>
<a class="participation bouton sinscrire" href='https://www.weezevent.com/exploration-zoom-sur-sentiments-et-besoins'>S'INSCRIRE</a>
<ul class="infospratiques">
 <li>Thème :</li> 
 <li><em>Zoom sur les émotions et besoins</em></li>
 <li><em>Dimanche 16 mai 2021</em></li>
 <li>14h à 18h en ligne</li>
</ul>
<h3 class='color-chameleon'>Au programme</h3>
<ul style="list-style: outside">
 <li> exploration des sentiments et des besoins : notre relation avec eux et leur identification </li>
 <li> entrainement à la traduction des pensées en sentiments et des sentiments en besoins</li>
 <li> espace d'accueil de ce qui sera vivant à l'instant "t" dans le groupe</li>
</ul>
<hr>
<!-- <a class="bouton sinscrire boutonsexplo" href='https://www.weezevent.com/initiation-a-la-communication-nonviolente-dj'>S'INSCRIRE</a> -->
<ul class="infospratiques">
 <li>Thème :</li> 
 <li><em>Écoute de soi et de l'autre</em></li>
 <li><em>Date à venir</em></li>
 <!-- <li>14h à 18h en ligne</li> -->
</ul>
<h3 class='color-chameleon'>Au programme</h3>
<p>
 <ul style="list-style: outside">
  <li> auto-empathie : des clés pour affiner une écoute de ses ressentis et pouvoir les lire </li>
  <li> écoute de l'autre : les freins à l'écoute, les identifier et les comprendre</li>
  <li> espace d'accueil de ce qui sera vivant à l'instant "t" dans le groupe</li>
 </ul>
</p>
<hr>
<h2 class='color-orange'>Les explorations de la Communication NonViolente, pourquoi ?</h2>
<p>
    À la suite des découvertes, il est habituel de vivre des difficultés pour ancrer la pratique de la CNV dans le quotidien. En proposant des moments d'exloration plus longs, notre intention est de créer des espaces pour aller plus loin dans la compréhension de la CNV, pratiquer ensemble à des endroits où cela peut être compliqué, et poursuivre la créativité sur comment vivre toujours plus de CNV au jour le jour.
</p>
<h3 class='color-orange'>On garde</h3>
<p>
      Un espace participatif où nous nous proposons de vous partager la Communication NonViolente à travers notre vécu, nos expériences et notre vision du monde.
</p>
<p>
    Pour nous, avant d'être un outil, une méthode, la Communication NonViolente est une philosophie de vie qui nous invite à croire en la coopération et l'abondance.
</p>
<h2 class='color-sky-blue'>Tarif et soutien</h2>
    <p>
    Le tarif de 50€ minimum permet de soutenir nos actions et de continuer à mettre de l'énergie dans la transmission de la CNV.
    </p>
    <p>
    Toutefois nous gardons notre envie de rendre la CNV accessible au plus grand nombre, ainsi nous ne souhaitons pas rajouter aux contraintes horaires et techniques des contraintes financières. C'est pourquoi nous vous invitons à nous contacter si ce tarif n'est pas accessible, auquel cas nous vous proposerons un tarif différent.
    </p>
          <div style="text-align: center">
     <h3 class='color-chameleon'><strong>Vous souhaitez soutenir la démarche, c'est ici</strong></h3>
     <a class="bouton plc" href='https://lydia-app.com/collect/71818-cnv-julie-diane/fr'>PARTICIPATION CONSCIENTE ET SOUTIEN</a>
    </div>
    <hr>
    <h2 class='color-scarlet-red'>Notre couleur particulière</h2>
     <h3 class='color-orange'>Qui sommes nous ?</h3>
    <p>
     Nous sommes deux femmes passionnées des relations interpersonnelles avec le doux rêve de changer le monde ! Dans la mouvance des différents mouvements de NonViolence, nous cherchons à incarner le changement que nous souhaitons voir dans le monde. 
    </p>
    <p>
     Fières du chemin que nous avons parcouru depuis notre découverte de la CNV nous essayons de transmettre cette manière de voir le monde, avec tout ce que nous sommes : nos forces, nos failles, notre système de croyance... Nous parions sur le fait qu'ensemble nous sommes plus fort·e·s, c'est donc AVEC les personnes qui viennent que nous créons un instant présent différent à chaque fois.
    </p>
   <div style="text-align:right">
     <strong>Diane Martin</strong> & <strong>Julie Quillé</strong>
    </div>
     <h3 class='color-orange'>Particularités de notre transmission</h3>
    <p>
     Nous croyons en l'apprentissage par l'expérience ! Nous essayons donc, dès l'initiation, de vivre la CNV : c'est à dire <strong>partager ce qui est vivant</strong>, et <strong>chercher ensemble comment nous rendre la vie plus belle</strong>.
    </p>
    <p>
     Cette façon de faire un peu originale par rapport à ce dont nous avons l'habitude peut être : curieuse, dérangeante, joyeuse, inconfortable, source de confusion selon chacun·e.
    </p>
     <h3 class='color-orange'>Ce que nous ne faisons pas</h3>
    <p>
     <ul>
        <li>Nous ne vous transmettrons aucune certitude, aucune vérité absolue.</li>
     </ul>
     Par ailleurs, nous ne sommes pas formatrices certifiées en CNV, vous pourrez trouver des <a href='https://www.cnvformations.fr/'>formations délivrées par des formateur·rice·s certifié·e·s ici</a> et pouvons également vous aider à trouver des formations qui vous conviennnent si besoin.
    </p>
