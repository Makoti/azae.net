---
title: "Intelligence Collective, communiquer autrement"
description: Renforcer l'intelligence collective au sein d'une équipe
category: peoples
illustration:
  name: cover.png
tags: [communication, équipe]
presentation: |
  Cette formation de 3 jours est spécialement conçue pour renforcer l'intelligence collective au sein d'une équipe. Nous y travaillons notamment la communication pour agir sur les non-dits, l'expression de soi, l'écoute, les conflits. Le tout est accès sur une dynamique de recherche de solution et d'expérimentations.
objectives: |
  - Motivation Intrinsèques : Mieux se connaitre soi-même et son équipe
  - Introduction à la Communication NonViolente et débat constructif
  - Apprendre à écouter / se dire les choses (expérimentations cas concrets)
  - Construire un système de gestion de conflit
  - Initier l’amélioration continue
prerequists: Aucun prérequis
duration: La formation est dispensée sur trois journées de 7 heures (idéalement la 3 ème journée se déroule 4 à 6 semaines après les 2 premières)
resources:
  - name: Programme de formation
    url: programme-formation-intelligence-collective.pdf
  - name: CGV
    url: /assets/documents/Azae-CGV.pdf
---
