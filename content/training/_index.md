---
aliases:
  - /services/training/
title: Nos formations
description: >
    Voici les différentes formations que nous proposons.
    Nous pouvons vous accompagner sur les sujets suivants : Management, Communication NonViolente, Les méthodes agiles (Scrum, Kanban, eXtrem Programming, Lean, ...), Devops, Software caftmanship (TDD, BDD, refactoring, clean code, ...), Technique (Docker, Java, Go, Puppet, Python), L’intelligence collective, Lean startup.
show_on_index:
  order: 3
  icon: fa-graduation-cap
  color: color-sky-blue
  summary: >
    Contactez nous pour que nous puissions définir ensemble le mode d'accompagnement le plus approprié pour vous.
menu:
  left:
    parent: Services
  footer:
    parent: Services
---
