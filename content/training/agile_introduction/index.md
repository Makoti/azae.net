---
title: Introduction aux méthodes agiles
description: "Découvrir les méthodes agiles et en expérimenter des pratiques"
category: peoples
tags: [methods, beginers]
illustration:
  name: agile-basics.jpeg
presentation: |
  Cette formation de deux jours a pour objectifs de faire découvrir les bases de l'agilité : sa philosophie, ses pratiques, la raison d'être et comment l'implémenter. Elle est destinée aux équipes IT qui souhaitente entamer ou poursuivre une transformation vers l'agilité. Cette introduction à l'agilité s'adapte aux différentes équipes et reste une occasion pour tous de découvrir, d'approfondir ou d'enrichir sa conaissance de l'agiité et des différents possibles.
  La pratique se fera sur vos projets mais aussi dans des ateliers de simulations ludiques par exemple avec des Lego(c).
objectives: |
  - Acquérir les notions des origines de l'agilité (manifeste agile / valeurs / principes)
  - Différencier agilité et méthodes agiles
  - Apprendre la méthodologie SCRUM (vocabulaire / rôles / artefacts / événements)
  - Initier la mise en oeuvre de SCRUM
  - Construire un esprit d'équipe
prerequists: Aucun prérequis
duration: La formation est dispensée sur une journée de 7 heures
resources:
  - name: Programme de formation
    url: programme-formation-agilite.pdf
  - name: CGV
    url: /assets/documents/Azae-CGV.pdf
aliases:
  - services/training/agile-introduction.html
  - training/agile-introduction/
---
