---
title: Boutique
permalink: /sentiments-besoins/
description: |
    Boutique en ligne outils de CNV et de coaching
menu:
  right:
    pre: <i class="fa fa-shopping-cart" aria-hidden="true"></i>
    weight: 45
  footer:
    parent: Nous
---
<link href="/assets/julie.css" rel="stylesheet">
<div class="row">
<div class="col-md-12">
<!-- Commentaire -->
<!-- Todo
5. Mails type
6. Ajouter des liens vers les ateliers CNV : Pub - voir min height 200 initiation ne marche pas
-->
<ul>
 <li><a href='#produits'>Nos produits</a></li>
 <li><a href='#ateliers'>Ateliers thématiques</a></li>
 <li><a href='#demarche'>Notre démarche</a></li>
</ul>
<h2 id="produits" class="categories">BOUTIQUE ACTUELLEMENT EN PAUSE</h2>
<hr>
<h2 id="ateliers" class="categories">Nos ateliers thématiques</h2>
<hr>
<h3 class="titresh3">
Nos ateliers découverte
</h3>
<img alt="Bonhomme OSBD" class="img-produit"src="OSBD-ini.png" width="145px"/>
<div class="description-longue">
<p>
Nous proposons régulièrement des ateliers de découverte de la Communication NonViolente, au cours desquels nous échangons autours des sentiments, des besoins et de la manière dont nous les articulons pour avoir un impact dans nos relations à nous même et aux autres.
</p>
<a href='https://azae.net/cnv/initiation/'>Retrouvez plus d'informations et nos dates.</a>
</div>
<hr>
<h3 class="titresh3">
Nos ateliers exploration
</h3>
<img alt="Bonhomme OSBD" class="img-produit"src="appro.png" width="145px"/>
<div class="description-produit">
<p>
Pour celles et ceux qui ont déjà découvert les principes de base de la Communication NonViolente, nous proposons des ateliers thématiques pour plonger dans un sujet plus prècis.
</p>
<a href='https://azae.net/cnv/approfondissement/'>Retrouvez plus d'informations et nos dates.</a>
<p>
Nous proposons également des ateliers sur mesures que ce soit dans les entreprises, les collectivités territoriales, les associations ou encore dans le milieu carcéral. Enfin tous les endroits au sein desquels améliorer la communication et la connaissance de soi a de la valeur.
</p>
<a href='https://azae.net/cnv/initiation/'>Abonnez-vous à notre nexsletter pour être informés des nouveautés.</a>
</div>
<hr>
<h2 id="demarche" class="categories">Notre Démarche</h2>
<hr>
<h3 class="titresh3">
Pourquoi ces produits ?
</h3>
<p style="list-style: outside">
Convaincus que le travail sur les émotions et les besoins est un puissant vecteur de développement personnel, utile et bénéfique dans tous les domaines de la vie, nous souhaitons faciliter le travail à toutes les personnes qui le désirent.

Notre démarche à commencer avec les cartes émotions et besoins de la FCPPF que nous affectionnons particulièrement et dont nous souhaitions faciliter l'accès en France.

Aujourd'hui, l'offre s'aggrandit en fonction des demandes que nous recevons et des produits que nous découvrons. Toujours en gardant en tête le côté accessibilité et transparence.
</p>
<hr>
<h3 class="titresh3">
Notre démarche
</h3>
<p style="list-style: outside">
Il y a quelques années nous avons découvert ces cartes mises au point par la Fédération des Centres Pluralistes de Planning Familial Belge. Ayant vu une grande utilitée dans cet outil et prenant un grand plaisir à les utiliser, nous avons été de fervants consomateurs de ces outils : pour nous, pour nos clients, pour offrir. Aujourd'hui, il nous est de plus en plus difficile de les avoirs à un prix accessible en France. Ainsi nous avons décidé de nous lancer dans cette démarche de faire des achats groupés, directement auprès de la <a href='http://www.fcppf.be/nos-outils-et-services-en-ep/nos-outils-et-publications/'>Fédération des Centres Pluralistes de Planning Familial Belge</a>, pour pouvoir proposer des prix proches de ceux de la <a href='http://www.fcppf.be/'>FCCPF</a>.
Voyant que cette démarche répond à un certain besoin, nous expérimentons de proposer d'autres outils complémentaires de ce que nous vous proposons.
</p>
<hr>
<h3 class="titresh3">
Quelques exemples d'utilisation
</h3>
<p style="list-style: outside">
Vous pouvez voir un exemple d'utilisation de ces cartes dans un atelier de travail sur les émotions au travail dans cet <a href='https://ajiro.fr/games/emotions_fortes/'>article sur les émotions fortes</a>.

Vous pouvez également essayer de faire l'exercice que nous proposons dans cette conférence de
<a href='https://www.youtube.com/watch?v=VJnXtoB1RAU'>présentation de la Communication NonViolente dans un contexte agile</a>.
</p>
<hr>
<h3 class="titresh3">
Transparence sur le prix des cartes
</h3>
<p style="list-style: outside">
Nous ne faisons pas de bénéfices sur la vente des cartes, ainsi vous pouvez calculer notre prix de vente en regardant le prix de la <a href='http://www.fcppf.be/nos-outils-et-services-en-ep/nos-outils-et-publications/'>Fédération des Centres Pluralistes de Planning Familial Belge</a> et le prix d'un envoi par colissimo. Les prix évoluent donc exactement de la même manière sur notre site et sur le site de la FCPPF.
Ce qui nous permet de garder le même prix que le prix en Belgique est une réduction que la FCPPF nous fait pour un achat groupé qui couvre les frais de port Bruxelle Paris.
Ainsi nous nous permettons également de vous demander de la clémence sur les délais de réponse ou d'envoi qui sont pris sur notre temps libre ;-)
</p>
<hr>
