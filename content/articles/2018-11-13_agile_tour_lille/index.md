---
date: 2018-11-13
title: "Agile Tour Lille 2018, c'est parti !"
draft: false
authors:
  - quille_julie
vie d'orateur:
 - Agile Tour Lille
themes:
  - Slow Kata
  - CNV
  - émotions

illustration:
  name: agile-tour-lille-2018.png
description: |
  Cette année, chez Azaé, nous avons été plus raisonnables sur le nombre d'Agile Tours auxquels nous avons participé. Bien qu'ayant fait une croix sur de nombreux événements que nous apprécions, nous ne pouvions pas manquer Agile Tour Lille.
---

Cette année, chez Azaé, nous avons été plus raisonnables sur le nombre d'Agile Tours auxquels nous avons participé. Bien qu'ayant fait une croix sur de nombrex événements que nous apprécions, nous ne pouvions pas manquer Agile Tour Lille. Un événement "un peu comme à la maison", que nous attendons tous les ans avec impatience.

Keynotes, conférences aux thèmes variés, une ambiance et des discussions qui nous font travailler notre capacité à faire des choix, et toujours ces moments que nous aimons, où, en tant qu'orateurs, nous pouvons partager nos passions. Ce sont deux deux jours intenses que nous partageons avec vous ici.

## Azaé orateurs passionnés

<img src="agile-tour-lille-2.jpeg" class="img-fluid" alt="Atelier les émotions au travail"/>

Lors de cet événement, nous avons été très heureux de pouvoir présenter des facettes très différentes de l'agilité. Tout d'abord, [Alexis](https://azae.net/team/) et [Thomas](https://azae.net/team/) ont présenté [Ralentir pour aller plus vite !](http://ajiro.fr/talks/slow_mob_programming/), une occasion de revenir sur l'exercice de partage et de reflexion qu'est la programmation. Ouvert à tous, développeurs et non développeurs, l'objectif de cet atelier est d'analyser les pratiques pour faire des choix en conscience. L'occasion de réveiller les passions chez les uns et les autres. Ce qui tombait à pic, puisque Thomas n'a eu que le temps de la pause pour changer de salle, et aller rejoindre [Julie](https://azae.net/team/) pour l'atelier [Les émotions au travail : qu'est-ce que j'en fais ?](http://ajiro.fr/talks/atelier_cnv/). Un atelier où nous invitions les participants à regarder de plus près les émotions, les nôtres mais aussi celles de nos collègues. Un exercice qui ne manque pas de défis, mais nous pouvons vous le dire, les 25 participants de l'atelier ont été incroyablement aventureux.

## Des keynotes qui remuent

<img src="agile-tour-lille-4.jpeg" class="img-fluid" alt="Keynote d'Emilie sur la Cargo Cult"/>

Cette année des keynotes qui font sensation. Pour commencer, [Agile Tour Lille](http://2018.agiletour-lille.org/programme) a fait fort en invitant Jurgen Appelo pour la Keynote d'ouverture. L'occasion de travailler un peu son anglais pour avoir accès au contenu passionant de cette conférence menée avec enthousiasme au sujet de l'aspect organique de l'entreprise. Frédéric Leguédois, en showman aguerri, et Emilie Esposito, en défenseuse de la recherche de sens, sont venus compléter ces points culminants de l'Agile Tour avec des talks qui nous rappellent les bases de l'agilité.

## Une ambiance que nous aimons

<img src="agile-tour-lille-1.jpeg" class="img-fluid" alt="Un lieu magnifique"/>

Thomas nous racontait en [vidéo](https://azae.net/articles/2018-11-06_agile_tour_lille_thomas/), la semaine dernière, l'affection bien particulière qu'il a pour cet Agile Tour, affection qu'il a su transmettre aux autres membres d'Azaé. Ainsi, aller à Lille à la rencontre des agilistes, plus ou moins locaux, c'est, pour nous, un peu comme une réunion de famille... en mieux.
Une grande partie de l'Agile Tour se déroule hors conférences, lors des repas et des pauses plus ou moins officielles, la journée et aussi une grande partie de la nuit. Ces échanges informels sont autant d'occasions d'ancrer les apprentissages, d'approfondir les sujets et de créer du lien, car ne l'oublions pas, changer le monde, c'est aussi une histoire de communauté.

Pour finir, un petit mot sur le lieu. Le cadre de l'Université Catholique de Lille a donné un cachet très particulier à l'événement, même si les salles n'ont pas toujours pu accueillir l'ensemble des personnes qui accouraient pour participer aux différents ateliers et conférences.

