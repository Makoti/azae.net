---
date: 2018-10-23
title: "L'expérience du feedback positif !"
draft: false
authors:
  - quille_julie
tags:
  - embarquement
  - coaching
  - atelier
  - Solution Focus
  - Speed Boat
illustration:
  name: feedbackpositif.jpg
description: |
 Donner du feedback ? On en parle toujours un peu, on se dit qu'on maitrise... Mais sait-on réellement donner du feedback, et surtout : en voyons-nous l'intérêt à sa juste valeur ?
---

Vous vous dites peut-être que vous n'avez pas besoin d'apprendre à donner du feedback positif, soit parce que vous savez déjà le faire, soit parce que c'est inutile.
Figurez-vous que la semaine dernière, encore imprégnée de mon expérience d'un weekend Communication NonViolente sur la gratitude, la nécessité du feedback positif en entreprise m'est apparue de manière évidente comme une des bases de la vie d'équipe.

## Remplie de croyances, je me mets en mouvement

Je fais partie de ces personnes qui pensent que trop d'expressions positives dénaturent la relation... le biais ? Je souhaite des relations tellement authentiques, qu'il est possible que je ne sois pas très familière avec la notion de gratitude. Il était temps que je m'attaque à ça.
En tant que coach, il me semble essentiel d'être attentive en permanence à mon système de croyance et de mettre en place des actions pour m'assurer de le remettre en question. Ici la décision fut simple : "Julie, le stage sur la gratitude ne te parle pas ? (oui quand même ça fait un peu bisounours etc.) - Il faut que tu y ailles !"

## Un weekend pour apprendre le caractère essentiel du feedback positif

Me voilà donc embarquée pour un samedi ET un dimanche de formation ! Vous connaissez, on se présente, on est timide, on l'est un peu moins, on parle de choses et on expérimente.
Et bien mesdames et messieurs, il ne me fallut pas moins de ces deux jours, plus un petit temps de retour en entreprise, pour faire un lien d'une évidence telle que je n'arrive pas à voir comment il avait pu m'échapper jusque là : l'expression de la gratitude, c'est du feedback positif ! Et pour aller un peu plus loin, le feedback positif devrait être une des premières étapes de toutes les démarches de changement agile.

## Une expérience surprenante pour changer ma vision du monde

Vivre le feedback positif, que ce soit en le donnant ou en le recevant, m'a permis d'expérimenter physiquement l'impact que cela peut avoir sur la façon dont nous voyons le monde. Pour ma part, à travers l'expérimentation, je me suis rendu compte qu'il m'était devenu possible de revisiter tout ce que m'apportaient mes relations professionnelles. Pas seulement le contexte, mais comment chacun individuellement était source de motivation, de challenge, d'apprentissage, d'inspiration et de support. Vous imaginez que cette prise de conscience m'a amenée à requestionner ma façon de voir le monde.

## Le feedback positif pour changer l'entreprise

Vous voulez changer votre vie en entreprise : faites des feedbacks. Vous voulez survivre au passage d'équipe sans feedback à équipe avec feedbacks : faites des feedbacks positifs.
Un des problèmes que nous rencontrons régulièrement dans les équipes, c'est que nous ne savons pas nous dire les choses. Nous voyons le souci de ne pas parler des problèmes et les conséquences que cela engendre. Et pourtant comment pouvons nous nous dire ce qui ne va pas alors que nous n'avons pas encore appris à sortir de notre appréhension de dire ce qui va bien !
C'est avec cette idée en tête que je suis allée animer des ateliers sur le feedback positif et figurez-vous qu'il y a du travail.

Alors un conseil, faites du feedback positif : mais pas n'importe comment. Et ne vous inquiétez pas, moi aussi, avant, je faisais partie de ces personnes qui pensaient que trop d'expressions positives dénaturent la relation.
