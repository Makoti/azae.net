---
date: 2019-04-17
title: "Agilité les mains dans les poches"
draft: true
authors:
  - quille_julie
vie de coach:
 - matériel
 - kit agilité
illustration:
  name: materiel_agile.jpg
description: |
    Si vous vous étes mis à l'agilité dernièrement, ou êtes coach agile, vous connaissez sans doute la course aux post-it, à la brosse pour tableau blanc... qui d'ailleurs n'efface pas le tableau blanc ! Plusieurs solutions s'offrent à vous : agiliste tout terrain, parrés à toute éventualité, vous vous promenez avec votre maison de l'agilité sur le dos : post-it géants, feutres, timer, scotch, rien ne peut vous surprendre ! agiliste minimaliste, toujours un bic et un bloc de post-it qui trainent au fond du sac; après tout ce qui compte c'est le cerveau, d'ailleurs l'illisibilité du tout entraine tout un chacun à travailler sa mémoire ! ou encore agiliste malin, vous venez les mains dans les poches ; l'agilité est avant tout un travail d'équipe, il y aura bien quelqu'un qui va se charger du matos... et sinon c'est que c'était pas si important !
---

Si vous vous êtes mis à l'agilité dernièrement, ou êtes coach agile, vous connaissez sans doute la course aux post-it, à la brosse pour tableau blanc... qui d'ailleurs n'efface pas le tableau blanc ! Plusieurs solutions s'offrent à vous : agiliste tout terrain, parés à toute éventualité, vous vous promenez avec votre maison de l'agilité sur le dos : post-it géants, feutres, timer, scotch, rien ne peut vous surprendre ! agiliste minimaliste, toujours un bic et un bloc de post-it qui traînent au fond du sac; après tout, ce qui compte c'est le cerveau, d'ailleurs l'illisibilité du tout entraîne tout un chacun à travailler sa mémoire ! ou encore agiliste malin, vous venez les mains dans les poches ; l'agilité est avant tout un travail d'équipe, il y aura bien quelqu'un qui va se charger du matos... et sinon c'est que c'était pas si important !
Aujourd'hui je me propose de vous partager mes fondamentaux du matériel agile, ce qu'en tant que coach tout terrain j'ai toujours dans mon sac. Je vous proposerai également les petits plus qui peuvent faire toute la différence.

## Les basiques :

Dans les basiques, rien de plus simple : post-it et feutres, de quoi brainstormer, travailler chacun de son côté et mettre en commun, regrouper, séparer, sélectionner, enfin c'est un miracle, d'ailleurs les concepteurs sont malins, ils sont déjà découpés et auto-collants pour que nous puissions mettre toute notre énergie neuronale au service du sujet !

### Post-it :

> post-it : les plus utilisés en basique sont les 76 x 76 mm et les 76 x 127 mm

Quantité suffisante : toujours plusieurs paquets d'avance ;
Plusieurs couleurs : les couleurs, au-delà d'être joli, c'est souvent utile pour différencier les étapes, idées, personnes etc. ;
Marque : si vous avez le choix, soyez attentif à la marque, si a peu près toutes les marques se sont alignés sur la taille, ce n'est pas le cas de la qualité et les réunions où nous passons la moitié du temps à ramasser les papiers qui tombent ne sont pas des plus agréables.

Et si vous êtes passionnés des post-it, vous pourrez trouver de nombreuses informations sur le choix et l'utilisation sur le net ^^.

### Feutres et marqueurs :

> feutres épais 6 à 8 mm pour les post-it

Des feutres épais pour tout le monde : l'idée d'écrire sur des post-it est de pouvoir partager nos idées, écrit en tout petit avec un stylo bille, c'est dommage. Et se passer les stylos c'est pas pratique.

> marqueurs pour tableaux blancs

Plusieurs c'est bien, un par personne c'est trop, quelques pochettes d'avance c'est malin !
Les couleurs basiques : même logique que pour les post-it, quelques couleurs différentes sont une grande aide pour segmenter la pensé.
Qui écrivent : élémentaire me direz-vous, et pourtant, les feutres dont ne ne voyons plus l'encre au-delà de 6 cm du tableau sont monnaie courante, l'investissement dans une nouvelle pochette de feutre est vite rentabilisé.

### Support :

> tableau blanc en bon état, post-it géants, les murs ce n'est pas toujours suffisant

> spray nettoyant pour tableaux blancs

Accessible et utilisable : vous connaissez votre environnement, si la lutte pour des centimètres carrés de tableau blanc vierge fait partie de votre quotidien, investissez dans des post-it géants ou un spray nettoyant. Un support utilisable : ce n'est pas en option.

### Scotch repositionnable :

> scotch de peintre ou autre scotch repositionnable

Repositionnable : la garantie de pouvoir l'utiliser sans abîmer les supports
Permissif : le petit plus, pas indispensable mais que j'aime à mettre dans les basiques, il permet de passer d'agiliste minimaliste à agiliste tout terrain pour un investissement financier et en espace moindre. Ce scotch permet entre autre de coller des papiers au murs (toutes les taille, même ceux nativement non enduit de colle !), délimiter des espaces, faire des liens, attacher un collègue à sa chaise, et bien d'autres utilisations que vous découvrirez au cours de votre pratique.

## Les petits plus qui peuvent faire toute la différence :

Ne l'oublions pas, le matériel ne fait pas l'agilité, il reste cependant un bon moyen de facilité le travail de chacun, après tout l'agilité c'est aussi ça.

### Bâton de parole :

> objet sympa appartenant à l'équipe : balle, bâton, peluche, laissez libre court à votre imagination

Indispensable pour les agilistes qui travaillent en permanence leur dynamique de groupe (nous ne rentrerons pas ici dans le début du caractère oxymorique ou non de cette phrase), le bâton de parole est un outil simple et très puissant pour expérimenter de nouvelles façons de faire.

### Timer :

> timer qui permet de définir et de rendre visible le temps qui passe

Dans la même dynamique que la bâton de parole, ce sont des outils que nous avons usuellement peut l'habitude de voir dans nos réunions, et pourtant les équipes qui s'y mettent ont tendance à voir des transformations spectaculaire de leur espace commun.

### Gommettes :

> gommettes rondes de différentes couleurs 10 mm

Vote, alerte, état, personne etc. ; les gommettes rajoutent un nouvel axe de différenciation complémentaire aux couleurs. A quelques euros le miliers de gommettes, c'est un petit plus dont il est dommage de se priver.

### Rubans pour tableau blancs :

> ruban pour tableau blanc 3 mm de largeur noir

Particulièrement utile pour les agilistes qui créent un management visuel physique (nous ne rentrerons pas ici dans le début du caractère oxymorique ou non de cette phrase ;-) ), les rubans pour tableau blancs, mais pas que, permettent facilement de délimiter des espaces de manière nette et propre.

### Plus de :

> post-it, feutres, scotchs, gommettes, stickers, rubans, lieux : allons dans la démesure

Plus de tailles et de couleurs différentes, plus de choix. Un ami qui m'est cher dit : "Tant qu'il n'y en a pas trop, nous ne sommes pas certain qu'il y en ai assez". Donnez-vous le droit de faire différemment, d'innover de créer. Un espace et un matériel diversifié, joli et facile d'accès est un catalyseur à innovation.


Évidemment cette liste n'est pas exhaustive et vous aurez tout le loisir de la compléter tout au long de votre. Passionnée de Communication NonViolente, mon méteriel ne saurait être complet sans un lot de cartes "sentiments et besoin", Thomas ne saurait se déplacer sans un moving motivator dans son sac, là où Olivier fera demi-tour s'il a laisser son assesment game sur la table au moment de partir ! A vous de créer votre kit de base, et surtout n'hésitez pas à partager ce qui marche pour vous !
