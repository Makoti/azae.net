---
date: 2017-10-24
title: "Histoire d'un article Azaé qui arrive sur InfoQ"
draft: false
authors:
  - quille_julie
vie d'Azaé:
  - publication
themes:
  - rémunération libre

illustration:
  name: InfoQ_Remulibre.png
description: |
  Publier des articles est pour nous une façon de pouvoir partager notre expérience, nos connaissances, nos réflexions auprès d'un public plus large que les équipes que nous accompagnons. En octobre dernier un de nos articles à dépassé les frontières de nos sites [Azae.net](https://azae.net/) et [Ajiro.fr](http://ajiro.fr/) pour aller se promener du côté d'[InfoQ.fr](https://www.infoq.com/fr/) : une aventure pour nous aussi.
---

## Pourquoi des articles ?

  Publier des articles est pour nous une façon de pouvoir partager notre expérience, nos connaissances, nos réflexions auprès d'un public plus large que les équipes que nous accompagnons. En octobre dernier un de nos articles à dépassé les frontières de nos sites [Azae.net](https://azae.net/) et [Ajiro.fr](http://ajiro.fr/) pour aller se promener du côté d'[InfoQ.fr](https://www.infoq.com/fr/) : une aventure pour nous aussi.

## Histoire d'un article à la vie trépidante :

  Notre article sur la rémunération libre est une histoire de valeurs, de conviction et surtout de travail ensemble. Né d'un travail commun d'Azaé ([Thomas](http://ajiro.fr/authors/clavier_thomas/) et [Olivier](http://ajiro.fr/authors/albiez_olivier/)) avec [Mija](http://ajiro.fr/authors/rabemananjara_mija/), une [première version de l'article](http://ajiro.fr/articles/2017-01-26-remuneration_libre/) a été publiée. Lorsque [Julie](http://ajiro.fr/authors/quille_julie/) est arrivée à Azaé, nous avons repris le travail, une manière pour nous d'enrichir nos points de vue et d'apprendre à travailler ensemble dans différents contextes : une deuxième version est arrivée. Ce fut également l'occasion de faire [un talk](http://ajiro.fr/talks/remuneration_libre/) autour de ce même sujet qui nous passionne. La vie de cet article aurait très bien pu s'arrêter là, et aurait déjà bien rempli son rôle de divulgation d'idées. Mais il faut croire qu'il avait de bien plus grandes ambitions puisque nous avons été sollicités pour écrire un article pour InfoQ, une étape riche pour nous tous.

## Dernière étape : InfoQ

  Toujours partants pour de nouvelles aventures et ne loupant jamais une occasion de travailler avec de nouvelles personnes, nous nous sommes lancés dans l'aventure d'une écriture pour un média qui n'était pas celui dont nous avions l'habitude. Ce fut un ensemble d'allers-retours entre nous et Stéphane pour adapter notre sujet et notre façon d'écrire aux standards d'InfoQ. C'est aussi apprendre à travailler différemment et grandir encore dans notre capacité à apprendre et à nous adapter. Le chemin valait le coup et le résultat nous ravit : [La rémunération libre, ou comment mettre l'auto-organisation au coeur de l'entreprise](https://www.infoq.com/fr/articles/la-remuneration-libre).
