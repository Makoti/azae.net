---
date: 2023-05-11
title: "Azaé de l’entreprise horizontale à l’ entreprise ouvertement politique : histoire d’une raison d’être"
draft: false
authors:
  - quille_julie
themes:
  - entreprise
  - gouvernance
  - politique
  - raison d'être

illustration:
  name: people.png
description: |
  Azaé est avant tout une entreprise humaine expérimentale. Le chemin se fait un pas à la fois, avec des apprentissages autant dans nos réussites que nos échecs. Avec patience, amour et soutien, nous créons cette aventure depuis plus de 10 ans, aujourd'hui nous vous partageons un de nos petits pas sur notre raison d'être : histoire d'hommes et de femmes, et de passion.
---

## Un nouveau départ en petits comités

On est en 2022, sortie du Covid et de quelques années de difficultés à s’aligner en interne. Thomas, le fondateur d’Azaé nous annonce son intention de quitter l’entreprise.

Cela nous laisse à 2. Un bouleversement, mais aussi l’occasion d’un nouveau départ. De nouveaux possibles émergent : que veut-on faire d’Azaé ? Quelles sont nos intentions, nos ambitions, notre raison d’être ? On cherche, on expérimente et, évidemment on se fait aider.

Notre première étape est de créer un ensemble d’espaces d’échanges à 3 : Olivier, Julie et une personne extérieure qui nous inspire. Nous invitons des personnes avec qui on aurait du plaisir à travailler. On ne sait pas encore mettre les mots, mais on sait que si on les trouve, les mots justes seront ceux qui nous parlent à nous, mais aussi aux personnes qui sont là.

On sait qu’on souhaite rester une entreprise horizontale, valeur présente dans Azaé depuis sa fondation. Mais aussi qu’on cherche « à créer un espace de prise de tête », autrement dit « qu’on cherche à pouvoir résoudre ensemble des problèmes que nous n’aurions pas pu avoir seul ». Dit comme ça ce n’est pas glamour pour tout le monde, mais on y croit, et on y tient.

## Nouvelles personnes, nouvelles idées, nouvelles dynamiques

Ce n’était pas gagné d’avance mais notre pitch touche, donne envie, et nous passons d’un cercle de 2 à un cercle de 4. C’est un début de cheminement, un potentiel à faire grandir, mais l’intégration de deux nouvelles personnes dans le groupe nous pousse à nous poser de nouvelles questions, clarifier des implicites, réfléchir à nos structures.
Les 29 et 30 aout 2022 nous organisons notre premier séminaire. Officiellement dans Azaé, ou aspirants, tout le monde réfléchi ensemble. Ces deux jours nous permettent de poser les bases : comment intégrer une personne, comment l’exclure, quelle est notre charte relationnelle, nos ambitions ? Résultats de nos inspirations diverses, de nos envies et de nos expériences : les premières pierres sont posées. L’une d’elle : la gestion explicite de tension n’est plus laissé à la subjectivité. Azaé se positionne : tout membre qui refuserait de la médiation alors qu’elle lui a été demandé, acte par là son retrait des décisions d’Azaé, y compris sa potentielle sortie.
Deuxième séminaire : nous sommes 5 (3 aspirants), troisième séminaire, nous sommes 6 (nous sommes maintenant 3 officiels, et 3 aspirant.e.s) : l’aventure commence vraiment.

## Un an plus tard : eureka

En mars 2023 nous sommes à notre 3ème séminaire. Nous avons mis en place des règles de fonctionnement avec des statuts différents. Nous expérimentons sur comment fonctionner avec des niveaux d'implications, de liberté et de responsabilités différents. Il y a celleux qui sont dedans et prennent la charge du fonctionnement de l’entreprise, celleux qui sont dedans en passager.e, apprenti.e capitaine, aspirant.e ou simplement observateurice.
Ce troisième séminaire regroupe des personnes de chaque catégorie, des personnes qui sont là depuis les débuts, et des personnes présentent pour la première fois. Au cœur de tout ça, un séminaire qui a l’odeur d’une aventure qui commence vraiment pour nous, qui débroussaillons l’entreprise libérée depuis des années et qui savons qu’entre l’intention, le geste et le résultat, il y a régulièrement des univers différents.

### Des traditions qui commencent à s’installer :

Nous prenons de longs moments de gestion de tension. Des plus ancien.ne.s au dernier.e.s arrivé.e.s tout le monde partage, tout le monde reçoit. Ces instants sont de réels régulateurs internes, qui permettent de poser les choses, les mettre à jour, et les transformer. Une réelle opportunité de vivre les tensions comme des cadeaux. (Quand même notre idée initiale de se prendre la tête ensemble avait de sacrément belles racines)

### Des pratiques qui émergent et se concrétisent :

Depuis plusieurs années Azaé réfléchi à mettre une part de son chiffre d’affaire de côté pour soutenir des associations et autres organisations actrices de changement sociale. Mais entre peurs, contraintes techniques, difficultés à accepter que de l’argent sorte de nos rémunérations pour aller ailleurs, le passage à l’acte s’est toujours fait de manière timide au mieux, voir pas du tout. Aujourd’hui c’est acté dans le principe : 2,5% du CA d’Azaé ira à des organisations extérieures. Les premières discussions ont lieux et 1 000€ sont donnés immédiatement aux caisses de grèves (on est en mars 2023, ne l’oublions pas).

### Un regard en arrière pour admirer le chemin parcouru

Azaé est, et a toujours été, une entreprise démocratique constituée de personnes qui ont envie de prendre soin les unes des autres et du groupe. Avec ce que ça apporte de meilleur et de pire. Dans le pire que nous avons pu expérimenter c’est l’inertie. L’inertie de l’absence de consensus, l’inertie du consensus mou, l’inertie de l’épuisement d’un statut quo trop puissant et trop stable.
Le redémarrage est également l’occasion de regarder en arrière. C’est avec une émotion particulière que nous avons ressorti le dernier compte rendu mensuel d’Azaé en date de juin 2020. C'est sur un partage de grande impuissance et d'insécurité que ce termine cet exercice. Le constat de cette difficulté de communication et d’appartenance qui s’étaient installés est un grand moment de prise de conscience du chemin parcouru.

## L’émergence d’une raison d’être

C’est cette année de cheminement qui nous permet de définir notre raison d’être, la description de qui nous sommes. Les séances de réflexions sur le sujet, mais aussi ces moments partagés et vécus où on a pu se dire « oui c’est ça » ! En l’occurrence un moment clé fut la question posée « Mais pourquoi voulons-nous grandir ? » et une des réponses qui y a été apportée : « Parce qu’un jour, nous voulons que ce soit 200 000€ qui puissent être donné aux caisses de grève. ». Le rêve partagé émerge : oui nous souhaitons changer le monde, oui nous sommes engagé.e.s, oui nous sommes politique. Azaé est une entreprise engagée et politique, qui souhaite vivre et promouvoir le faire ensemble. Se "prendre la tête" fait partie du chemin, autant dans les obstacles qui apparaissent inévitablement, que dans les intentions d’acquérir les compétences pour y faire face, pour en faire des occasions de grandir. Oui Azaé est un endroit où l’on se "prend la tête", parce que nous ne croyons pas qu’il est possible de mettre nos tripes dans un projet, de nous engager, de porter des valeurs, de le faire ensemble ET d’éviter le conflit à tout prix. Et enfin oui Azaé est un endroit où on "se prend la tête", parce que ces "prises de tête" comme on dit, ce sont des moments d'authenticité, de soin, d'explicitation des non-dits. C'est ce qui améliore notre connaissance de nous et des autres, c'est l'occasion pour chacun.e d'être vu dans nos singularités. Et croyez le ou non, ce sont des moments de liens et de partages intenses.
Aujourd’hui c’est ce projet que nous proposons aux personnes qui veulent nous rejoindre, et un jour nous l’espérons, ce sera un élément différenciant pour nos clients. Mais dans tous les cas le chemin nous fait grandir, et nous savons qu’il ne fait que commencer.

Derrière chaque “non” il y a un “oui”. Identifier ce “oui” maximise nos chances de mettre en place des stratégies gagnant-gagnant.

Si j’arrive à augmenter ma capacité à dire “non”, j’augmente le nombre de demandes que je peux entendre. Si j’augmente le nombre de demandes que je peux entendre, j’étends ma zone de sécurité et je réduis ma colère. Si je réduis ma colère : je suis plus libre 😀 .
