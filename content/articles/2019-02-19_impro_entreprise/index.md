---
date: 2019-02-19
title: "Une entreprise théatrale"
draft: false
authors:
  - quille_julie
vie d'Azaé:
  - interne
  - nos valeurs
illustration:
  name: impro-lifi.jpeg
description: |
  Ces dernières années, les serious game se démocratisent de plus en plus, le team-building se développe dans tous les domaines, et nous parlons de "gamifier" les apprentissages. Convaincus que cette démarche est un réel changement de paradigme, mais aussi que nous ne devons pas oublier l'intention derrière la forme choisie, chez Azaé, nous sommes plusieurs à creuser cette voie.
---

Ces dernières années, les serious game se démocratisent de plus en plus, le team-building se développe dans tous les domaines, et nous parlons de "gamifier" les apprentissages. Convaincus que cette démarche est un réel changement de paradigme, mais aussi que nous ne devons pas oublier l'intention derrière la forme choisie, chez Azaé, nous sommes plusieurs à creuser cette voie. Pour ma part, je me suis lancée dans le théatre d'impro, je vous raconte ici cette histoire.

# Pourquoi cet article

Il y a maintenant un peu plus d'un an, je découvrais le théatre d'impro avec l'[université du Nous](http://universite-du-nous.org/). Un stage de 2 jours dans lequel nous avons travaillé sur différents aspets de cet art particulier. Les liens avec l'entreprise et le travail que nous faisons auprès des équipes m'a paru évident. Le travail qu'il me restait à faire à titre individuel également ! Ce fut une raison suffisante pour que je me lance dans l'aventure de cours hebdomadaire avec la [LIgue Française d'Improvisation](https://www.improvisation-lifi.com/) (LIFI). Le weekend dernier, nous avons donné notre premier spectacle, l'occasion pour moi de vous partager quelques uns des parallèles qui sont, pour moi, source d'inspiration.


# Des apprentissages hors des bancs

Quelle que soit votre place au sein de votre entreprise, vous avez sans doute déjà eu plusieurs occasions de faire des "serious games". Vous savez, ces petits jeux qui se glissent de l'entretien d'embauche à la formation, en passant par le team-building... entre autre. Ce nouveau mode d'apprentissage présente de nombreux avantages : mobiliser plusieurs formes d'intelligences, retrouver le plaisir du jeu dans le monde professionnel, favoriser l'imagination et l'autonomie, mais également accroitre la motivation, l'implication et la possibilité d'apprendre dans un cadre sécurisé. Chez Azaé, convaincus que les meilleurs alliés d'une entreprise sont des employés passionnés par ce qu'ils font, nous utilisons beaucoup les serious game. Cependant, ne voulant pas perdre de vue l'objectif pédagogique en ne faisant que surfer sur la vague d'une mode passagère, nous avons, chacun dans des domaines différents, appronfondi ce qui se cache derrière le jeu. Aujourd'hui je ne vais pas vous parler de lego ou de post-it, mais du jeu théatral de l'improvisation. Le rapport avec l'entreprise ? Il y en a de nombreux.

# Une équipe, un objectif commun

Lorsque vous êtes sur scène, l'objectif est, avant tout, la satisfaction des spectateurs. Ainsi, vous pouvez avoir été parfait, être venu en sauveur et avoir été très pertinent, si votre jeu n'est pas au service de vos coéquipiers, toute l'équipe en ressort perdante et vous serez juste le héros d'une troupe médiocre. Au contraire, en improvisation, pas le temps de s'accrocher à notre égo, l'urgence est dans ce qui se passe ensemble.

# Sortir du jugement, faire des propositions

Tout est histoire de proposition ! Lorsque vous rentrez en scène, vous n'avez aucune idée de ce qui va se passer, la clé du succès : être à l'écoute de ce qui se passe autour de soi. Si le groupe est en difficulté, à vous de venir avec une proposition claire, précise et forte. En quelques secondes vous devez réussir à faire passer votre intention, et si elle n'est pas attrapée par vos partenaires, vous avez à peu près autant de temps pour faire le deuil de votre idée et suivre le groupe... même si l'idée choisie ne vous semble pas être la meilleure.

# L'erreur source d'inspiration

En théatre d'improvisation, comme dans beaucoup de jeux, l'erreur fait partie du jeu. Les raisons peuvent être multiples mais la seule question importante est : voulez-vous vous arrêter sur l'erreur, chercher qui a raison qui a tort et prouver le bon droit de chacun, ou faites-vous en sorte que le spectacle continue ? Ainsi, si vous passez aux répétitions de la [LIFI](https://www.improvisation-lifi.com/), un soir en semaine, vous pourrez voir un groupe de 15 personnes se féliciter de leur plantage ! Etrange, me direz-vous ! Et pourtant, cela permet de dédramatiser l'erreur, d'apprendre de cette erreur plutôt que de mettre notre énergie à essayer de la cacher et de se réjouir des efforts que nous avons déjà fait pour en arriver là.

# Une réunion théatralisée

L'entreprise n'est pas une pièce de théatre, me direz-vous ! Et pourtant, n'avez-vous jamais rêvé d'une réunion avec un groupe focalisé sur un objectif commun, à l'écoute les uns des autres, étant tous capables de lâcher prise sur les résultats projetés pour pouvoir se concentrer sur une création collective ? Une réunion dans laquelle les propositions sont claires et les erreurs source d'inspiration ? Au final, nous voilà avec de l'improvisation bien cadrée. A quand, dans nos entreprises, un cadre favorisant davantage les régles de l'improvisation ?
