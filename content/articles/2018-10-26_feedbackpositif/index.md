---
date: 2018-10-26
title: "Articles Feedbacks"
draft : false
authors:
  - quille_julie
vie d'Azaé:
- publication
outils :
- feedback
- CNV
description: |
  Nous vous avons raconté mardi comment le feedback et en particulier le feedback positif avait pris un rôle de choix dans notre accompagnement dernièrement. Aujourd'hui nous vous proposons de découvrir un atelier posible pour le travailler en équipe.
illustration:
  name: feedback-positif.png
---

Mardi, nous vous racontions les prises de conscience liées au travail sur la gratitude dans l'article [L'expérience du feedback positif !](https://azae.net/articles/2018-10-23_atelier_feedback_azae/). Et comme toujours, nous essayons de vous partager également la méthodologie que nous avons suivie pour l'intégrer dans nos coachings. Pour ceux que cela intéresse vous trouverez la description de l'atelier [Feedback Positif](http://ajiro.fr/games/feedbackpositif/) sur [Ajiro](http://ajiro.fr/).
