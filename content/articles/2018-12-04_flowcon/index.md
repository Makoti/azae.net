---
date: 2018-12-04
title: "A la découverte de FlowCon"
draft: false
authors:
  - quille_julie
vie d'orateur:
 - FlowCon
themes:
  - diversité
  - émotions

illustration:
  name: flowcon-2018.png
description: |
  La semaine dernière nous étions à la première édition de la Flowcon. Adéptes des conférences et des échanges, participants attentifs et animateurs passionnés, c'est Azaé dans toute son ampleur qui a repondu présent à ces deux jours de rencontres autour de l'agilité.
---

Nous connaissions LeanKanban FRance (LKFR) et nous aimions. Nous aimions tellement que nous nous sommes précipités sur les billets dès l'ouverture de la billeterie de la [FlowCon FR](http://flowcon.fr/). C'est donc, non sans fierté, que nous sommes arrivés dès le mardi matin, à 4, prêts pour deux journées intenses.
Un grand succès pour cette première édition de FlowCon FR feu LKFR.


<img src="equipe-azae.jpeg" class="img-fluid" alt="Azaé à la FlowCon"/>

## Discussion autour du Flow

Un changement de nom ne se fait pas sans attiser les curiosités. Je vous passerai les détails de ce que j'ai compris du pourquoi du comment, mais j'ai une envie particulière de commencer par la fin avec une dédicace toute particulière à Edouard avec sa conférence [Ce jour où je ne serai plus coach Agile](http://flowcon.fr/sessions/ce-jour-ou-je-ne-serai-plus-coach-agile/), dont on retrouve en grande partie les notions clés dans son [article](http://www.arolla.fr/blog/2018/06/jour-ne-serai-plus-coach-agile/). Cette conférence qui a clos l'événement avec passion et authenticité nous disait que, victime de son succès, l'agilité avait perdu de sa pureté. Différenciant les multiples facettes de l'application de l'agilité et leurs déclinaisons pour chacune d'elles (craft pour les programmeurs, devops pour les ingénieurs, product management pour le marketing produit), les "méthodes" ont récuperé le graal et, elles, s'appellent "agile". Mélange d'une partie et du tout, les acteurs du quotidien deviennent les grands oubliés de l'agilité. Edouard nous propose de redonner une juste place à une agilité englobante, et, pour les facettes partielles, renommer l'agilité des organisations : Flow.
La boucle est bouclée ; il n'y avait pas de plus jolie clôture possible pour ce bel évenement.

## De nombreuses conférences Flow

[Emotional Safety](http://flowcon.fr/sessions/emotional-safety/) avec Pawel Brodinsky, [Leadership and the art of listening](http://flowcon.fr/sessions/active-listening-and-clean-language/) ou encore [Modèles mentaux](http://flowcon.fr/sessions/maitrise-personnelle/) avec Béatrice Arnaud, ne sont que quelques unes des nombreuses conférences Flow de très haut niveau auquelles nous avons pu participer. Nous avons aimé les thématiques : très axées humains et savoirs être, la qualité des interventions : précision des informations apportées et aisance des orateurs, mais aussi, chose rare, la sincérité avec laquelle bon nombre d'orateurs se sont mis à nu sur scène. Nous ressortons plein d'apprentissages et de gratitude.

## Partenariat pour discuter de diversité


Puisque j'ai eu le plaisir d'être également oratrice dans cette conférence, en plus de l'expérience de participant, j'ai envie de vous partager mon expérience de ce point de vue là. Lors de la FlowCon, j'ai une fois de plus eu la joie de répéter l'initiative lancée il y a quelques mois avec Raphaël d'/ut7. En effet, nous avons co-animé [La parole au travail : un espace de privilège masculin](http://flowcon.fr/sessions/le-genre-et-la-parole-au-travail/). Un moment privilégié pour les femmes pour partager leur vécu au travail... mais pas que ! Pour les hommes, un moment pour pouvoir s'immiscer dans des conversations de femmes, écouter en toute transparence ce que nous sommes prêtes à nous dire sur ce sujet dont l'importance est encore bien trop souvent sous-estimée. Pour tous, une occasion d'apprendre, de prendre conscience, de mieux nous connaitre, et, nous l'espérons, d'avancer vers plus de conscience. Un grand merci à toute l'équipe de FlowCon pour l'attention portée aux orateurs, au choix des sujets et à la confiance qu'ils nous ont manifestée en choisissant un sujet tel que celui-ci.
