---
date: 2019-02-12
title: "Agile Open France une année de plus"
draft: false
authors:
  - quille_julie
vie d'orateur:
  - Agile Open France
outils :
  - Forum Ouvert
illustration:
  name: AOF_hiver_2019.png
description: |
    Cela revient régulièrement et tant mieux ! Janvier est, comme chaque année, l'occasion de partir d'un bon pied avec quelques jours de Forum Ouvert. Nous y étions, et nous en revenons ravis.

---

[Agile Open France](https://agileopenfrance.com/) : nous vous en avons parlé il y a quelques mois, pour vous raconter la session [AOF été](https://azae.net/articles/2018-07-06_agile-open-france/). Première fois que nous vous en parlions sur le site, et pourtant cet événement, vieux de plus d'une décade, est un moment clé dans la vie d'Azaé. Tout d'abord pour le plaisir que nous y prenons, les amis que nous y retrouvons, les apprentissages avec lesquels nous repartons, mais aussi comme lieu de rencontre d'agilistes convaincus et expérimentés avec des valeurs proches des nôtres. Une fois de plus nous en revenons ravis !

## Les deux pieds du Forum Ouvert

<img src="loi-des-deux-pieds.jpg" class="img-fluid" alt="La loi des deux pieds."/>

Vous êtes peut-être familiers de ces moments de Forum Ouvert, la loi des deux pieds n'a pas de secret pour vous ! Quel bonheur ! Pour les autres : il n'est jamais trop tard pour apprendre.
La loi des deux pieds, c'est l'idée selon laquelle, à l'intérieur d'un espace temps défini, nous allons faire ce qui est important pour nous et nous laisser porter par nos pieds pour chercher où, quand, comment et avec qui nous allons répondre à cette invitation à être attentif à ce que nous voulons vraiment.
Dite différemment, cette régle nous invite à passer à autre chose dès lors que nous ne sommes ni en train d'apprendre, ni en train de contribuer.
Cela semble simple et pourtant c'est un exercice, qui, à lui seul, pourrait nous occuper une vie entière. De notre côté, nous l'aurons expérimenté avec une cinquantaine d'autres, pendant 96h, réparties sur 5 jours.

## Agilité une manière d'être

Plus j'y participe et plus je vois dans AOF une conférence où l'agilité est avant tout une manière d'étre. Cette conférence Agile vous parlera très peu de SCRUM, de rétros, de poker planning. Par contre, vous y trouverez des sujets touchant aux coopératives, au féminisme, à la philosophie, à la liberté. Peut-être me demanderez-vous le rapport à l'agilité ? Il est partout, dans la façon de voir le monde, de co-construire un moment, de mener ou de ne pas mener les échanges, de comprendre l'univers d'autres personnes. AOF c'est le seul endroit où je pense qu'il est possible pour une personne d'intégrer l'agilité sans avoir à en parler. Et comme chez nous l'agilité c'est avant tout une [manière d'être](https://ajiro.fr/articles/2018-07-04-agile-sans-conscience/), nous sommes heureux d'y être.


## Une édition particulière

"Pour moi aussi c'était mon premier Agile Open France Hiver 2019". Ce mot de la fin, donné par un participant, m'a fait réaliser que l'expression "une édition particulière d'AOF" est un pléonasme. Il n'y en a jamais deux semblables. Et même si nous retrouvons quelques éléments clés, des amis, un lieu, c'est une nouvelle aventure à chaque fois, où nous découvrons rarement ce à quoi nous nous attendions. Pour moi cette année, ma grande découverte a été autour de la demande et du non. Un sujet que je couvre régulièrement avec la Communication Non Violente et qui pourtant reste plein de mystère. Mon axe de cheminement personnel pour 2019 est trouvé, rendez-vous l'année prochaine pour le bilan.
