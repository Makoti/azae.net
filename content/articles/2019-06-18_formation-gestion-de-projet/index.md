---
date: 2019-06-18
title: Formation de gestion de projet
draft: false
authors:
  - clavier_thomas
illustration:
  name: postit-board.jpg
description: |
  On m'a récemment demandé ce que je proposerais comme plan de formation pour assurer les deux modules de la partie "Gestion de projet agile" de l'IUT informatique.
  Cette demande faisant écho à une demande client, je profite de cet article pour synthétiser et partager le plan de la formation "Gestion de projet agile" que nous proposons à nos clients.
---

On m'a récemment demandé ce que je proposerais comme plan de formation pour assurer les deux modules de la partie "Gestion de projet agile" de l'IUT informatique.
Cette demande faisant écho à une demande client, je profite de cet article pour synthétiser et partager le plan de la formation "Gestion de projet agile" que nous proposons à nos clients.

Le déroulé de la formation est le suivant :

- Ancrage historique
- Vue d'ensemble des rôles, responsabilités et vocabulaire
- Gestion produit
- Production
- Leadership

Enfin, nous tenons à souligner que dans une démarche de transformation vers plus d'agilité, comme l'ensemble des rôles et des responsabilités changent, nous cherchons à former l'ensemble des acteurs.

## Un peu d'histoire

Avant de parler gestion de projet agile, il est important de revenir sur une vision plus classique, afin de bien comprendre le pourquoi de l'émergence de l'agilité.

Retour sur la vision de Taylor et Ford : une personne est capable d'orchestrer et piloter un projet depuis l'analyse du besoin jusqu'à la livraison.
Pas forcément compétent sur toutes les phases, il est néanmoins capable de faire travailler les gens, de découper finement le travail, d'affecter les tâches et d'anticiper l'ensemble des problèmes.
Une sorte de marionnettiste du projet, doté d'un leadership plus ou moins grand en fonction de la taille de ses équipes.

### Cycle en V et méthode Cascade

Vision analytique de la production d'un projet avec un début et une fin.

- Description rapide.
- Analyse des avantages et des inconvénients.

Conclusion, on observe des gens ou des équipes aux responsabilités bien séparées.

### Pourquoi l'agilité

- Défauts des méthodes traditionnelles.
- Complexité du monde.
- Rapidité.
- Promesses de l'agilité.
- Partis pris de l'agilité.

### Le manifeste agile

- Valeurs et principes.
- Contexte.

Pas de responsable unique ou de super chef d'orchestre mais une responsabilité collective.
Une organisation avec des personnes tournées vers le client et d'autres vers le produit.
Un objectif produit qui dure et pas des projets indépendants.
Des itérations très courtes pour aller chercher le feedback du client et s'adapter rapidement.

## Vue d'ensemble

Afin de partager et faire vivre les grands concepts de l'agilité nous commençons par un premier atelier Lego4Scrum ou Lego4Kanban. Cela permet la prise de conscience des rôles, des responsabilités collectives et du vocabulaire de base.

Dans une démarche agile, comme l'ensemble des rôles et des responsabilités changent, il est important de former l'ensemble des acteurs.

## Gestion produit

Souvent considéré comme la première étape d'un cycle. Elle concerne principalement les gens tournés vers le client et / ou l'utilisateur.

Il s'agit ici de travailler sur le comment comprendre le besoin utilisateur, le découper et le hiérarchiser.
Garder un backlog, ni trop gros ni trop petit, capable d'alimenter les personnes qui vont participer à la réalisation du produit.

Nous utilisons ici des ateliers comme :

- Event storming
- Lean startup
- Impact mapping
- Backlog grooming
- Artiste et spécifieurs
- Une prairie ensoleillée
- Design thinking
- Dans un contexte informatique nous rajoutons des notions de "Domain-driven design" (DDD) et de "Behavior-driven development" (BDD)

## Production

Nous arrivons à l'étape "faire". Même si, nous le répétons, la boucle doit être très courte et rapide.

Dans cette partie du cours nous abordons principalement les notions de :

- cycle court,
- production de valeur,
- mesure de la valeur produite (perçue, potentielle et réelle),
- amélioration continue et qualité, avec entre autre l'andon, le pdca,
- et nécessité de s'entourer d'experts engagés.

Dans un contexte de sous traitance nous apportons une attention particulière aux notions de lean management liées aux risques, à l'engagement et à la confiance.

Dans un contexte informatique, par exemple des étudiants d'IUT, nous parlons bdd, tdd, clean code, pair programming, automatisation, déploiement continu mais aussi ddd et Xtreme programming.

## Leadership

Le leadership est présent à toutes les étapes et indispensable mais de façon différente pour chacun des acteurs. Le leadership mais aussi la communication (la capacité à donner du feedback constructif, la gestion de ses émotions, la gestion des conflits, faire des demandes claires et accessibles, etc.)

Étapes de ce sous module :

- Différences entre leader et manager avec un atelier Lego serious play.
- Communication NonViolente
- Feedback constructif
- Eole
- Solution focus
- Host leadership
- Delegation pocker
- Moving motivator
- Conversations constructives

Nous allons aussi parler équipe, cohésion et engagement.

Pour ce genre de formation, nous utilisons en plus les ateliers suivant :

- Flowgame
- Stylo voyageurs
- Barre d'hélium


## Conclusion

Nous pouvons également aborder les notions d'agilité à l'échelle, c'est à dire comment faire pour que plusieurs équipes dépendantes les unes des autres arrivent à produire vite et bien.

Lors des formations, nous ne faisons jamais l'intégralité de ce que nous venons de vous présenter. Le contenu est adapté en fonction des objectifs spécifiques de chacun des groupes que nous accompagnons et, évidemment, évolue en cours de formation en fonction des objectifs, des contraintes et des observations faites.

Enfin, dans le contexte de l'IUT, organisant actuellement les 2 projets de seconde année comme une mise en pratique de l'agilité, compléter cet enseignement pratique par les 60 heures du module "Méthodologie de la production d'applications" serait une très bonne base.
