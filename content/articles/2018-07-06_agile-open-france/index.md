---
date: 2018-07-06
title: "Juillet 2018 : première édition d’Agile Open France Été"
draft: false
authors:
  - quille_julie
vie d'orateur:
  - Agile Open France
outils :
  - Forum Ouvert
illustration:
  name: agile-open-france-2018.png
description: |
    Vous l’aurez peut-être remarqué, depuis un petit moment j’essaye de vous raconter les confs auxquelles nous participons avec Azaé. Vous donner les points que j’aime (éventuellement ceux que j’aime moins), vous transmettre l’ambiance et nos intentions quand nous y participons.
    Suite à notre séjour dans la campagne des Cévennes, un article s’imposait, après quelques semaines d’intégration de tout ce que nous y avons vécu, il est temps de coucher par écrit l’essentiel.

---

Vous l’aurez peut-être remarqué, depuis un petit moment j’essaye de vous raconter les confs auxquelles nous participons avec Azaé, vous donner les points que j’aime (éventuellement ceux que j’aime moins), vous transmettre l’ambiance et nos intentions quand nous y participons.

Suite à notre séjour dans la campagne des Cévennes, un article s’imposait. Après quelques semaines d’intégration de tout ce que nous y avons vécu, il est temps de coucher par écrit l’essentiel.



## Agile Quoi ???

<img src="dragobert.jpg" class="img-fluid" alt="Le baton de paroles le plus joli du monde."/>

Dans le tour des conférences agiles, [Agile Open France](https://agileopenfrance.com/) fait partie des immanquables ! Tellement particulière que j’ai même du mal à la considérer comme une conférence. Cet événement est loin de tout l’imaginaire que nous pouvons avoir autour de ce mot. Vous pensez peut-être « CFP », « speakers », « programme sur papier glacé », Agile Open France (AOF pour les intimes), n’est rien de tout ça. « Forum Ouverts », « Diversité », « Co-création » en sont les maîtres mots. Entre 30 et 80 personnes selon les éditions qui se retrouvent au même endroit, au même moment, pour faire… en fait on ne sait pas exactement quoi.


## Une édition toute particulière

Pour la première fois AOF a eu une édition « été ». Là où nous avions l’habitude de 3 jours en Alsace en hiver, nous avons expérimenté 4 jours en été dans les Cévennes. Les différences ? Un lieu bien plus grand et une invitation permanente, de par la localisation et de par la météo, à être dehors. Une parité femmes / hommes presque parfaite (de quoi faire rêver plus d’un-e agiliste). Et des thématiques encore plus atypiques qu’habituellement dans un contexte de « conférence Agile ». Mais ici un dicton s’impose « ce qui se passe à AOF reste à AOF », je ne vous en dirai donc pas plus.



## Azaé à AOF

<img src="azae-a-aof-2018.jpg" class="img-fluid" alt="Thomas et Alexis en pleine réflexion dans la grande salle à manger d'AOF"/>

Cela fait plus de 6 ans qu’Azaé participe systématiquement à AOF. Et comme certaines autres entreprises qui participent à l’événement, Azaé l’a bien compris : c’est un lieu privilégié pour recruter des personnes d’exception. Bon d’accord je ne suis pas forcément très objective puisque c’est lors d’AOF que j’ai rencontré Azaé et que ce fut le point de départ de l’aventure que nous écrivons depuis.
Cette année c’est Azaé au grand complet qui était présent parmis les quelques 36 participantes et participants qui ont fait le trajet pour le plaisir de se retrouver, l’espace de quelques jours à partager nos passions, nos connaissances, nos compétences et nos valeurs.


## Mes belles expériences d’AOF

<img src="programme.png" class="img-fluid" alt="Le programme co-construit d'AOF"/>

AOF fut de nouveau l’occasion de participer à des ateliers sur l’égalité hommes femmes, des occasions pour moi d’avoir des témoignages particulièrement enrichissants d’hommes et de femmes qui abordent la question avec leurs couleurs personnelles. Un format d’atelier que nous allons de nouveau proposer en binôme, notamment à la [Flowcon](https://flowcon.fr/) de cette année.
Un travail particulier sur la parole, au sens de l’écoute et de la dynamique que cela engendre. Pas de nouveauté pour moi, mais une occasion supplémentaire de constater à quel point les habitudes peuvent avoir la vie dure, ainsi que les conséquences que cela peut avoir. Je repars notamment avec un réseau de personnes désireuses de poursuivre l'entraînement sur Paris. Une jolie suite pour ces moments hors du temps.
Enfin, et je le garde pour la fin car cela reste mon petit plaisir personnel, un très bel échange autour de la Communication NonViolente, avec un voyage au coeur des 4 étapes. Un moment fort pour moi qui accompagnait, pour la personne accompagnée, mais aussi pour les autres participants qui ont pu voir le processus à l’oeuvre.

En ces temps de canicule, nous avons une raison de plus de nous dire : « vivement l’édition d’hiver ».