---
date: 2018-09-13
title: "Embarquement d'une nouvelle équipe"
draft: false
authors:
  - quille_julie
vie de coach:
  - accompagnement
outils:
  - speedboat
  - solution focus
rituels:
  - embarquement
illustration:
  name: speedsf1.jpg
description: |
 Premier atelier d'accompagnement vers toujours plus d'Agilité. Des outils, un process, un résultat, comment nous essayons d'allier fond et forme pour faire de l'agile en étant agile.
---

  Un nouvel accompagnement, toujours cette petite appréhension, à la manière d'un acteur qui entre en scène, et un questionnement "Comment embarquer l'équipe de la manière la plus efficace possible". Les individus et leurs interactions plus que les processus et les outils, ça demande de la créativité, de l'adaptabilité et de l'énergie ! Beau challenge sans cesse renouvelé que le début d'un coaching.

## Objectifs :

 * Impliquer et donner envie.
 * Créer du lien et de la confiance.
 * Avoir une forme d'engagement réciproque.
 * Trouver de l'inspiration pour la suite.

# Solution Focus

  Depuis mes débuts chez Azaé [Solution Focus](https://coachingleaders.co.uk/what-is-solution-focus/) fait partie de nos sujets de discussions mais aussi de nos outils. Une façon très riche d'être dans l'accompagnement sans nuire à la souveraineté de la personne que nous accompagnons, tout en l'aidant à trouver ses propres solutions.

## Pourquoi Solution Focus
  L'idée de Solution Focus, c'est d'enquêter davantage avec un langage de la solution qu'avec celui du problème. Nous retrouvons des idées connues : faire avancer les choses, mais avec l'intention de voir ce que nous pouvons faire de différent pour nous améliorer plutôt que de comprendre pourquoi nous n'avons pas eu le résultat sur lequel nous comptions.

## Les intérêts dans un début de coaching
  Mon objectif était ainsi de pouvoir créer une dynamique de groupe agréable, tout en rentrant dans le vif du sujet rapidement, et en ayant une idée de la direction que nous allions prendre ensemble : coach et équipe. Avec cette vision, cet outil en tête et toute la motivation du monde je restais bloquée sur comment utiliser Solution Focus en groupe, dans un temps relativement court tout en ayant des informations intéressantes. Et comme tout bon coach qui se respecte, toute bloquée que j'étais, j'ai appelé un autre coach.

# Brainstorming etc.
  Une étape qui me passionne toujours est l'étape de création d'un moment de coaching. Venir vers un collègue avec une problèmatique, des contraintes, des envies, et avoir une personne, au service de l'idée, qui vient enrichir, compléter et challenger le matériel de départ.
  À [Azaé](http://azae.net), cette étape de co-construction nous semble essentielle. D'abord parce qu'elle rend les idées plus riches et plus solides ; nous bénéficions de plusieurs points de vue. Ensuite parce qu'elle nous permet d'incarner le faire ensemble ; nous nous habituons à l'altérité, la stratégie n'est plus uun point d'encrage, tout en apprenant à maintenir nos objectifs. En bref nous apprenons à faire et à être ensemble.

  Ici le Brainstorming est thé, il a apporté le [Speedboat](http://coach-agile.com/speed-boat/) dans l'équation (je vous fais grâce des circonvolutions empruntées pour en arriver là). Une sorte de rétrospective de ce qui se passe jusqu'à présent, de là où nous souhaitons aller dans le futur (préféré) et de ce que nous voyons pouvoir faire pour la prochaine étape. A ce stade les habitués de Solution Focus devrait déjà voir là où nous voulons en venir.

# SolutionFocus prend le large

<img src="speedsf2.jpg" class="img-fluid" alt="Joli dessin de speedboat solution focus"/>

  Nous en sommes arrivés à l'idée d'un SpeedBoat orienté solution, avec attention à la segmentation de la pensée. En concret qu'est ce que ça donne :
  Une première étape au cours de laquelle nous imaginons le futur préféré, notre objectif, notre île. Le tout sous la forme de "Qu'est-ce que je verrais de différent, si demain je me réveillais et que je mon équipe était encore plus top qu'actuellement (mais genre plus plus plus)".
  Dans la deuxième étape, nous regardons tout le chemin que nous avons déjà parcouru. "Nous ne sommes déjà plus à quai, qu'est-ce qui aujourd'hui fait que nous avançons ?".
  La dernière étape avait pour objectif de décrire le prochain petit pas. Le prochain stop entre là où nous sommes et notre île. Elle s'est déroulée en deux temps, d'abord chacun identifie 1 ou 2 différences qu'il verra quand nous serons au prochain stop. Ensuite nous regroupons les post-it pour qu'il ne reste que 2 à 4 points dans lesquels tout le monde se sent impliqué et sur lesquels nous allons nous concentrer.

# Ce que ça a donné

  Nous avons fait notre atelier en une heure environ, nous avons eu le temps de faire chacune des étapes en prenant notre temps, nous avons imaginé des possibles sur les différents points (sans prendre aucun engagement d'action, ça faisait partie du contrat de départ).
  Nous partons dans le coaching, avec trois axes de travail prioritaire dans lesquels toute l'équipe se retrouve et qui sera notre base de travail.
  Personnellement je ressors de cet atelier avec une vision plus claire de la dynamique de ce groupe, des idées de points de blocages et de pistes de solutions. Et la cerise sur le gâteau : j'ai hâte d'y retourner.
