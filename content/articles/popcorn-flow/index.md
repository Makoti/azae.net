---
title: Popcorn Flow
date: 2015-11-03
authors:
  - albiez_olivier
contributors:
  - Thomas Clavier
  - Damien Salvador
outils:
  - popcorn flow
illustration:
  name: illustration.jpg
aliases:
  - articles/popcorn-flow.html
---

Pour devenir une organisation apprenante, il y a plusieurs méthodes. Beaucoup passent par des rétrospectives. [Claudio Perrone] a mis au point une démarche pratique pour structurer cette démarche d'apprentissage qu'il a appelé [Popcorn Flow].
Le mot popcorn est composé des initiales de chacune des étapes en anglais : Problem, Option, Possible experiments, Committed, Ongoing, Review et Next.

L'idée principale est _"changer, c'est difficile, il faut donc le faire plus souvent"_. Pour cela, on va utiliser un kanban qui va piloter nos apprentissages / changements. Ce kanban pourra être suivi au daily meeting et mis à jour lors des rétrospectives.


## Les étapes


{{% img-left name="Problem.png" %}}
### Problème

Problem en anglais, le processus commence en identifiant des points qui bloquent ou qui gênent. L'objectif est d'identifier les problèmes. Il n'est pas nécessaire de factualiser, il suffit que le problème soit partagé par l'équipe.
{{% /img-left %}}


{{% img-left name="Options.png" %}}
### Options

Option en anglais, chercher plusieurs options pour adresser le même problème. Suivez la [Règle des 3](/articles/rule-of-three/) ou des [Real Options](http://www.agilecoach.net/coach-tools/real-options/), trouvez au moins trois options différentes.
{{% /img-left %}}


{{% img-left name="Experiments.png" %}}
### Expérimentation possible

Possible experiments en anglais, pour définir une expérience, il faut en préciser les actions à réaliser, sa raison, les résultats attendus et une durée.
{{% /img-left %}}


{{% img-left name="Committed.png" %}}
### Accepté

Committed en anglais, quand toute l'équipe accepte l'expérience, il faudra lui définir une date de revue et éventuellement un responsable.
{{% /img-left %}}


{{% img-left name="Ongoing.png" %}}
### En cours

Ongoing en anglais, la liste des expériences en cours...
{{% /img-left %}}


{{% img-left name="Review.png" %}}
### Résultats

Review en anglais, partage sur les résultats de l'expérience. Quelles étaient nos attentes ? Que s'est-il effectivement passé ? Qu'avons-nous appris ?
{{% /img-left %}}


{{% img-left name="Next.png" %}}
### La suite

Next en anglais, que faisons-nous après ? Le problème est-il complétement réglé ? Faut-il de nouvelles options ? C'est le moment de revenir au début du cycle.
{{% /img-left %}}


## Les points forts


### Real Options

Le principe de trouver un maximum de solutions différentes permet de libérer la créativité et évite d'approfondir la première idée qui apparaît. Ce n'est pas sans rappeler [Real Options] de [Pascal Van Cauwenberghe].


### L'expérimentation comme vecteur du changement

L'expérimentation est limitée dans le temps ou dans son périmètre. Elle a pour vocation de rapidement tester une option. Ainsi, parce qu'elle est limitée, elle suscite moins de résistance de la part des équipes. C'est un point crucial, surtout pour des décisions à l'unanimité.


### Le changement rendu visible

La colonne ongoing permet de voir le nombre de changements en cours et c'est donc un indicateur visuel des apprentissages et améliorations de l'équipe.


### Roue de Deming

[Adrien Piquot] dans son article [Vous reprendrez bien un peu de popcorn] fait remarquer à juste titre que le popcorn flow incarne une [Roue de Deming].
En effet, nous pouvons voir :

- Plan: Problem, Options, Possible experiments
- Do: Committed, Ongoing
- Check: Review
- Act: Next


---
Sources:

- Lean Kanban France 2015, Claudio Perrone, Popcorn Flow - Continuous Evolution Through Ultra-Rapid Experimentation
- <http://popcornflow.com/>

[Claudio Perrone]: https://www.linkedin.com/in/claudioperrone
[Popcorn Flow]: http://popcornflow.com/
[Real Options]: http://www.agilecoach.net/coach-tools/real-options/
[Pascal Van Cauwenberghe]: https://www.linkedin.com/in/pascalvancauwenberghe
[Roue de Deming]: https://fr.wikipedia.org/wiki/Roue_de_Deming
[Adrien Piquot]: http://blog.soat.fr/author/adrien-piquot/
[Vous reprendrez bien un peu de popcorn]: http://blog.soat.fr/2015/12/lean-kanban-france-2015-vous-reprendrez-bien-un-peu-de-popcorn/
[Règle des 3]: /articles/rule-of-three/
