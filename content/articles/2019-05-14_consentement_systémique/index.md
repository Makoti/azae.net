---
date: 2019-05-14
title: "Consentement systémique : ou comment identifier la proposition alliant le meilleur niveau d'acceptation"
draft: false
authors:
  - quille_julie
outil:
 - consentement systémique
 - protocole de décisions
illustration:
   name: choisir.jpg
description: |
    Le consentement systémique : une procédure de vote qui mesure l'objection, afin de choisir l'option qui a le niveau d'adhésion le plus grand. Ce n'est pas intuitif, mais ça marche !
---

En 2017, à [Agile France](http://conf.agile-france.org/), j'ai pu assister à la conférence de [Thibault Bouchette](https://www.linkedin.com/in/thibaultbouchette/?originalSubdomain=fr) sur le consentement systémique : un protocole de décision que je ne connaissais pas. Cela fait donc plus de 2 ans que cette intention de coucher sur papier ce que j'en ai compris est présente ; aujourd'hui je passe enfin le ticket de ToDo à Doing !

## Les objectifs du consentement systémique

Le consentement systémique est un outil de décision en groupe. Il est un des process de vote possible dont l'objectif est de créer de la coopération. Pour ce faire, nous allons favoriser l'expression des objections afin de choisir l'option qui en cumul le moins. Cela va, de fait, favoriser l'option ayant l'acceptation la plus grande. C'est donc dans ce but, et afin de minimiser les résistances, que contrairement à un très grand nombre de protocole de vote nous allons mesurer le niveau d'objection plutôt que le niveau d'adhésion à une idée.

Cette mesure va se faire à travers une note de 0 à 10. Voilà comment cela se passe :

<img src="consentement_systemique.jpg" class="img-fluid" alt="Démarche de consentement systémique illustrée"/>

## Le déroulé du vote

1. Choisir l'objet du vote
1. Lister les différentes propositions (choisir une façon de les classer ; ordre alphabétique, ordre d'arrivée etc.)
1. Dans ces propositions ajouter "ne rien faire" ou "aucune des propositions"
1. Évaluer les propositions de 0 à 10, avec 0 = acceptation totale et 10 = résistance maximum
1. Calculer les totaux
1. Sélectionner la proposition avec le score le plus bas : la proposition avec la résistance la plus faible est la proposition qui a le meilleur niveau d'acceptation

## Et en vrai, qu'est-ce que ça donne ?

Un exemple concret pour voir comment ça marche :

## Les petits plus

### La solution passive : ne rien faire

La solution "ne rien faire" est une solution que j'aime intégrer régulièrement dans les options quelque soit le protocole de décision choisi. Par exemple, s'il vous arrive de croiser des équipes qui ne tiennent pas leurs engagements ;-)
L'idée est simple, mais puissante :

> *Toute proposition qui a plus d'objections que ne rien faire n'est pas digne d'intérêt.*

Cela permet également à chacun de prendre conscience que dire "nous prenons des engagements que nous ne tenons pas" revient à dire "nous ne faisons rien" et donc que le problème n'est peut-être pas si désagréable que ça.

### Le consentement express

Les additions de X nombres de 0 à 10 peut vite devenir un frein à ce vote (surtout pour des décisions que nous souhaiterions prendre rapidement !), mais tout a été pensé ! Il est possible de changer l'échelle de vote :

1. O main levée : pas d'objection
1. 1 main levée : objection faible
1. 2 mains levées : objection forte

Et n'oublions pas, nous avons comme postulat de base que l'être humain est bon, il peut être intéressant d'expliciter brièvement les objections fortes dans un premier tour et de refaire un vote ; lors du choix du restaurant du midi j'apprends que mon collègue est allergique aux cacahuètes et qu'*Au Roi de l'Arachide* il joue sa vie sur n'importe quel plat, cela risque fortement de monter mon niveau d'objection pour ce choix... même si pour ma part la cacahuète est mon aliment préféré et que je suis amoureuse du serveur ;-)

## Retours d’expérience

Il m'est arrivé de me lancer dans l'explication du consentement systémique sur une phrase de départ négative, avec des propositions formulées à la négative et voter avec l'objection. L'explication est devenue très vite remplie de confusion ! En effet, je ne veux pas, ne pas prendre de chaise pour ne pas changer le bureau, donc je veux prendre une chaise pour ne pas changer le bureau, ou est-ce je veux changer le bureau en ne prenant pas de chaise, et ça c'est ce que je ne veux pas... enfin avec le consentement systémique j'évite les "ne pas" !
