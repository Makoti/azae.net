---
date: 2019-02-26
title: "Alexis nous raconte La Conf' School of PO"
draft: false
authors:
  - quille_julie
vie d'orateur:
 - School of PO
themes:
  - culture produit
illustration:
  name: school-of-po-2019.jpg
description: |
    Aujourd'hui Alexis nous partage son expérience de La Conf' School of PO : ce qui l'a motivé, ce qu'il a aimé et ce qu'il en retient. Nous vous invitons à suivre ce parcours avec lui pour une journée plongée au coeur de la culture produit.
---

  Aujourd'hui [Alexis](http://draft.azae.net/team/) nous partage son expérience de [La Conf' School of PO](https://laconf.schoolofpo.com/) : ce qui l'a motivé, ce qu'il a aimé et ce qu'il en retient. Nous vous invitons à suivre ce parcours avec lui pour une journée plongée au coeur de la culture produit.

**Bonjour Alexis, peux-tu nous partager ce qui t'a motivé à aller à La Conf' School of PO qui a eu lieu mercredi 13 février dernier à Paris ?**

Tout d'abord, ce qui m'a motivé c'est la conférence d'Edouard Gomez-Vaëz [*Ce jour où je ne serai plus coach*](http://flowcon.fr/sessions/ce-jour-ou-je-ne-serai-plus-coach-agile/). Il y parlait de la nécessité, pour tout bon coach, de développer des compétences hors de son domaine d'expertise pour incarner le T-shape que l'on aime tant demander aux développeurs agiles. Cela m'a vraiment donné envie de venir à La Conf' School of PO dont il avait parlé. Et je dois dire qu'avec le programme annoncé, il m'a été facile de me motiver !

**Nous retenons donc la conférence d'Edouard et un programme au top comme sources de motivation ! En parlant de ça, pourrais-tu me dire ce qui a attisé ta curiosité dans ce programme ?**

Avant de venir j'avais déjà participé à un atelier d'Andrea Provaglio sur le Clean Langage et ça m'avait beaucoup plu, du coup j'ai eu envie de pouvoir assister à son atelier *Value Mapping*. La plénière de Thais Vauquières *Produit Comique* a également réellement attisé ma curiosité.

**Je comprends que tu es allé à La Conf' School of PO avec la perspective d'un bon moment. Et au final qu'est-ce qui t'a le plus marqué ?**

Le produit comique a été vraiment quelque chose de chouette pour moi. Thais, une comique, nous a joué un sketch de son spectacle, et nous a expliqué comment elle en était arrivé là, comment ça avait maturé dans sa tête, ce qui marchait pour elle. Ce qui est intéressant c'est qu'on voit un produit différent. On a l'habitude de parler des produits dans un contexte logiciel, et là, le voir dans un autre contexte nous permettait d'appréhender quelque chose de différent. Et les conversations en à côté, qui restent des moments passionnants.

**Tu arrives à me faire regretter de ne pas avoir été là jusqu'à la fin ! Et du coup si tu ne devais retenir que trois choses de cette journée, ce serait quoi ?**

C'est une question difficile ! Mais je vais essayer d'y répondre.
D'abord ce serait les Vanity Metrics dont nous a parlé Oussama Ammar. Les Vanity Metrics, ce sont ces métriques qui flattent notre égo. Elles sont inutiles pour gérer le business, on ne prend pas de décisions avec ! Par contre, en externe on communique uniquement avec ça. Je trouve cette dichotomie super intéressante.

Ensuite, je vais repartir avec l'histoire, racontée par Gojko Adzic, d'une entreprise dans laquelle les développeurs ont passé des mois à développer un système distribué temps réel pour se rendre compte au final qu'une conversation aurait pu permettre de mieux comprendre le besoin client et éviter beaucoup de développement. Je trouve que c'est une belle illustration de l'importance de la communication avec le client.

Enfin, ce serait la punchline d'Oussama Ammar qui dit "Nous ne sommes pas éduqués à être honnête, dans notre société ça s'appelle être malpoli et rude". J'ai trouvé ça très drôle.

**Je vois qu'il y avait des porteurs de grands messages :-) Pour la peine je vais terminer avec une question clin d'oeil à Olaf Lewitz qui était également conférencier à La Conf' School of PO et qui a fait réfléchir son auditoire sur ce qui comptait vraiment : qu'est-ce qui ne sera plus jamais pareil pour toi après cette journée ?**

Je pense que ça a changé ma vision du PO. Jusque là j'avais l'impression que c'était un gros mot et finalement je me rends compte que faire un bon produit, au delà de la technicité que ça demande ou des différentes méthodologies, c'est beaucoup de bon sens. Me rendre compte de ça va me permettre de regarder différemment la différence entre ce que les gens disent et ce qu'ils font.

**Un grand merci à Alexis pour ce partage d'expérience, et félicitations à La Conf' School of PO pour la richesse qu'ils ont su donner à cette journée.**
