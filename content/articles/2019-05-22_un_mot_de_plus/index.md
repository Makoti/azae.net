---
date: 2019-05-22
title: "Rétrospective : On se passe la balle"
draft: false
authors:
  - quille_julie
rituels:
 - rétrospective
illustration:
   name: un_mot_de_plus.jpg
description: |
    La rétrospective c'est simple ! Et pourtant, cela se passe rarement tel que décrit dans la littérature. Retour d'expérience sur un modèle de rétrospective que nous avons crée pour favoriser le dynamisme.
---

La rétrospective c'est simple ! Et pourtant, cela se passe rarement tel que décrit dans la littérature. Retour d'expérience sur un modèle de rétrospective que nous avons créé pour répondre aux problématiques suivantes :

- comment mettre davantage de dynamisme,
- comment balayer des sujets précis dans un temps court,
- comment apporter un peu de légèreté

## Du dynamisme en intro

Pour un ice-breaker qui mette tout le monde en mouvement et qui nous aide à penser différemment, nous avons choisi un exercice tiré directement de notre expérience du théâtre d'improvisation :

1. Se mettre debout, en cercle
2. L'animateur lance la balle à quelqu'un en disant un mot, par exemple *arbre*
3. La personne qui reçoit la balle la lance à une autre personne en disant un mot en lien avec le mot précédent, par exemple *forêt*
4. Continuer de cette façon pendant 5 minutes environ

Les règles :

- On ne donne que des mots qui n'ont jamais été dit
- Le mot doit avoir un lien avec celui qui a été dit juste avant
- Et... on va vite !

## La balle reste dans la salle

### Des questions ciblées

Pour la suite nous avons affiché une dizaine de questions au mur :

- Un point de l'orga que tu aimerais garder ?
- Un point de l'orga que tu aimerais changer ?
- Une chose agréable qui est arrivée depuis la dernière fois ?
- Une chose désagréable qui est arrivée depuis la dernière fois ?
- Une chose que tu as faite et dont tu es fier ?
- Une chose que tu aurais aimé faire différemment ?
- Un bon moment que tu as passé avec l'équipe ?
- Une chose désagréable qui est arrivée depuis la dernière fois ?
- Une chose que tu as apprise ou découverte ?
- Une chose que tu aurais aimé apprendre ?
- Une chose que tu attendais et qui n'est pas arrivée ?
- Une chose que tu as très envie de raconter là maintenant ?

Le choix des questions se fait en fonction de ce que nous souhaitons particulièrement travailler dans la rétrospective. Par exemple "Une chose que tu attendais qui n'est pas arrivée" peut aider à revenir sur les engagements pris et qui n'ont pas été tenus.

Une fois les questions affichées, nous gardons la disposition de l'ice-breaker : debout, en cercle, avec une balle.
Les membres de l'équipe se lancent la balle. L'envoyeur pose une question au receveur de la balle qui donne une réponse et l'envoie à quelqu'un d'autre avec une autre question.

Le ou les animateurs noterons les réponses données pour la collecte de ce qui s'est passé.

### Les points d'attention

1. Dans une équipe peu dynamique, les réponses peuvent être lentes. Dans ce cas nous proposons à ceux qui n'ont pas la balle de donner leur réponse à la question posée. La réponse du receveur sert alors de timer.
2. Nous laissons l'équipe s'auto-réguler sur les questions qu'ils souhaitent poser et à qui ils les posent, nous veillerons cependant à ce que chacun participe.
3. Nous avons fait cette rétrospective avec des personnes à distance : ce n'est pas toujours facile ^^

## La rétro suit son cours

Une fois que nous avons collecté les informations à l'aide de la balle et des questions nous poursuivons avec les étapes classiques, tout en essayant de garder le groupe debout (c'est clairement LE défis de cette rétrospective), et avec le même dynamise :

- regroupement des post-it par thématique
- vote pour les thématique sur lesquelles on va travailler
- collecte d'idées d'actions pour améliorer les thématiques sélectionnées
- choix parmi l'ensemble des actions proposées
- plan d'action avec le qui et le quand

## Bilan de l'expérience

Au final, c'est un modèle que nous avons suffisamment aimé pour avoir envie de vous le partager.

Les plus :

- les questions permettent de balayer plus de sujets lorsque les équipes ont du mal à trouver des choses à dire
- la rétrospective debout et avec la balle change la dynamique du groupe
- le modèle est simple et permet de s'adapter facilement à un timing réduit

Les difficultés :

- mettre en place et garder le dynamisme
- noter toutes les idées si le groupe rentre dans le jeu
- intégrer les personnes à distance

Bonne expérimentation à tous !
