---
date: 2020-03-19
title: "Coaching solidaire"
draft: false
authors:
  - clavier_thomas
  - quille_julie
illustration:
  name: cooperation.jpg
  description: Coopération sur un puzzle
description: |
    Notre petite participation à l'entraide nationale en cette période ce coronavirus.
---

Notre petite participation à l'entraide nationale en cette période ce coronavirus.

**Pourquoi** : vivre nos valeurs de coopération et de solidarité en participant à l'entraide et à la solidarité covid-19

**Comment** : profiter du confinement pour offrir des heures de coaching aux managers et autres encadrants dont les équipes subissent la situation actuelle et souhaitent bénéficier d'un peu d'écoute, d'accompagnement, etc.

**Qui** : un groupe de coachs personnels, professionnels et agile

**Quoi** : des sessions de coaching individuel à distance


* Vous souhaitez en profiter, [contacter nous](/contact/).
* Coachs, votre soutien, ainsi que vos contributions seront les bienvenus

