---
date: 2019-07-18
title: "Agile France 2019"
draft: false
authors:
  - clavier_thomas
vie d'orateur:
  - Agile France
illustration:
  name: bernache.jpg
description: |
    Cette année encore, nous étions tous présents à [Agile France](http://2019.conf.agile-france.org/) au millieu des bernaches du Canada au chalet de la porte jaune.
    Voici un rapide tour d'horizon de mes apprentissages.
---


Cette année encore, nous étions tous présents à [Agile France](http://2019.conf.agile-france.org/) au milieu des bernaches du Canada au chalet de la porte jaune.
Voici un rapide tour d'horizon de mes apprentissages.

## Un peu de craft

J'ai commencé ma journée avec une belle histoire, celle de Julien et de la transformation d'une équipe vue par le prisme de la technique. Elle était intitulée : "Sur le chemin de la qualité : en équipe et dès demain !"

À travers son histoire, on découvre sa stratégie d'accompagnement. 
Tout débute par une phase d'observation en immersion totale : très importante pour connaître l'histoire et éviter de proposer des choses déjà testées. Il met un focus sur les impacts observables de la non-qualité, à savoir : le time to market, les indisponibilités, les bug, le désengagement de l'équipe, la confiance dans le code, etc.

À la fin de la phase d'observation, il ressort une sorte de rapport d'étonnement avec les symptômes, les douleurs et les fiertés de l'équipe. 

Enfin, il va pouvoir lancer des actions de changement par petits pas.

L'histoire est belle, on voit que ça fonctionne, et Julien nous rappelle l'importance du groupe, de la progression par petits pas et de la valeur perçue par le client.

Les slides sont en ligne et disponibles sur [speakerdeck](https://speakerdeck.com/jak78/sur-le-chemin-de-la-qualite-en-equipe-et-des-demain)

## De la pédagogie agile

La session d'après, je suis allé voir Manuella, une enseignante que j'avais eu le plaisir de découvrir au Mans. Sa session était intitulée : "Pédagogie Agile : Et si l'implication, l'attention, la mémorisation et la gratitude se cultivaient ?". 

Première étape, un petit briseur de glace : Manuella nous a annoncé des critères de regroupement et nous a laissé nous organiser. On s'est d'abord regroupé par métier puis par boisson au petit déjeuner. J'ai trouvé astucieux de regrouper l'ice-breaker avec un bon moyen d'avoir une idée de la composition du public.

Seconde étape : imaginez 30 personnes avec chacun un mot écrit avec amour par la maîtresse sur un grand carton. Objectif : reconstruire un texte. Une fois le texte reconstitué (c'était le [manifeste de la pédagogie agile](https://pedagogieagile.com/2012/05/12/les-valeurs/)) nous avons tous lu le premier point et avant de nous partager son expérience en la matière, Manuella nous a demandé : "Ça vous évoque quoi ? Avez-vous eu des petites actions qui illustrent ce principe ?" Et là, il s'est passé un truc fabuleux, chacun a pris conscience qu'il était déjà acteur de la pédagogie agile. Chaque participant a trouvé un exemple de "je le fais déjà un peu". Après ça Manuella nous a ouvert les yeux vers d'autres possibles.
Dans cet atelier, j'ai vu un formidable moyen de changer le monde, tout le monde était sur l'échelle de "Solution Focus" en train de se dire "je suis déjà là", "je peux faire quelque chose", et nous avons co-construit avec Manuella un futur préféré.

La troisième étape était sur l'implication et l'engagement. À l'image de l'atelier précédent, la fierté des premières réussites génère l'envie d'apprendre plus. En s'appuyant sur des victoires, on peut générer de l'engagement et de l'implication pour aller plus loin. Mais il faut apprendre à regarder en arrière, apprendre à fêter nos victoires. C'est pour ça que Manuella a ritualisé la porte des réussites dans sa classe. Régulièrement, les élèves prennent le temps d'écrire leur petite victoire et les affichent dans la classe.

Quatrième étape, s'entraîner à dire et à recevoir des mercis. Promouvoir la psychologie positive : ça se travaille ! Recevoir un merci ce n'est pas toujours facile, dire merci à quelqu'un alors que tout ne va pas, c'est difficile. C'est pourquoi il est important de s'entraîner. Comme l'artisan s'entraîne à parfaire son geste, dans la classe de Manuella. Les enfants s'entraînent à dire et à recevoir des mercis à travers des cartes "Chaudoudoux" écrites et dessinés par les enfants eux même. Nous avons partagé là un beau moment d'émotion.

Cinquième et dernière étape, le retour au calme de la classe. Manuella utilise la méditation en pleine conscience. Seulement imaginez une classe de 26 enfants surexcités à l'idée de partir en vacances à qui on demande de faire 20 secondes de méditation. Et bien cela fonctionne ! Car toute la classe est entraînée deux fois par jour à méditer quelques secondes. Là encore en plus de l'intérêt premier de l'exercice, apprendre à concentrer son attention, l'entraînement est mis à l'honneur.

Nous avons terminé l'atelier avec un pliage que vous pouvez d'ailleurs télécharger sûr [griffonotes](https://griffonotes.com/wp-content/uploads/2019/03/kawot_p%C3%A9dagogieAgile.pdf)

## Forum ouvert

Cette année, alors que j'allais tranquillement profiter du soleil et des amis que je n'avais pas vus depuis longtemps, j'ai été happé par l'atelier de [Nils](https://twitter.com/Nils_Back) sur le modèle [Agile Fluency](https://www.agilefluency.org/) et l'utilisation qu'il en fait en atelier.

Alors que je ne suis pas d'accord avec le modèle, j'ai trouvé vraiment très intéressant l'usage qu'il en fait en atelier. Par exemple comme argument d'autorité pour expliquer aux clients pressés que le changement qu'ils veulent faire ne va pas arriver en 2 semaines, ou comme description d'un futur préféré que le client n'avait pas envisagé.

## Du fret ferroviaire

Sans jamais avoir été un grand fan de train, je garde malgré tout une fascination pour ce système d'une grande complexité avec des problèmes totalement insoupçonnés et qui malgré tout arrive à déplacer des millions de gens chaque jour. La session suivante était intitulée "Préserver la planète par le fret ferroviaire, un modèle d'innovation orientée résultats" l'occasion pour moi d'en comprendre quelques éléments supplémentaires. Ce qui a été une grande source de plaisir. 

En plus de ce plaisir, je repars de cette conférence avec : une prise de conscience de la différence entre innover vs inventer ainsi que la découverte de l'échelle [TRL de la Nasa](https://www.nasa.gov/directorates/heo/scan/engineering/technology/txt_accordion1.html).

## Un jeu d'agilité à l'échelle

Scale Puzzle Game : Comment construire un puzzle de plus de 1000 pièces à plusieurs ... Le pitch m'a totalement embarqué, l'atelier beaucoup moins. L'objectif des animateurs était de nous faire vivre des expériences pensées et réfléchies à l'avance pour nous amener à une organisation de grand groupe plus agile. C'était pour moi très frustrant, c'est comme si on m'avait demandé d'éplucher une pomme avec les ongles puis une autre avec un couteau et que l'on me disait en conclusion : c'est mieux avec le couteau. Oui, je le sais sauf que nous étions très nombreux et que j'aurais bien aimé profiter de l'occasion pour tester plein d'autres choses.

Je retiens une chose pour mes futurs ateliers : il est très difficile de s'adapter aux compétences et aux attentes de tous les participants. Contraindre les joueurs pour les amener à simuler un truc en particulier, c'est potentiellement frustrant alors que les fédérer derrière un objectif collectif avec de nombreux observateurs pour assurer le méta, permet d'apprendre ensemble et à son rythme.

## Live Refactoring de Legacy Code

Pour me détendre de la précédente frustration, j'ai eu le plaisir de regarder une restructuration de code en direct avec les techniques du Golden Master et du Mikado. Je ne connaissais pas la technique du mikado et j'aime beaucoup. Je cherche encore comment je pourrais l'exploiter différemment.

## Xtrem Reading

J'ai découvert à travers cet atelier un exercice extrêmement riche pour appréhender collectivement et très rapidement un grand nombre de livres.

Je vous livre tel quel la recette.

En groupe de 2 à 5, prendre chacun un livre différent. 
Pendant 15 minutes chacun parcourt son livre et prend des notes avec l'intention de le présenter aux autres. 
Au bout des 15 minutes, les livres tournent (passage au voisin de droite). 
Puis on recommence l'exercice de survol du livre avec prise de notes. 

On va refaire l'opération autant de fois que de personnes dans le groupe, chacun aura parcouru tous les livres. 
On va alors entrer dans une phase de présentation collective du premier livre, chacun apportant son point de vue. Une fois le livre présenté, on peut passer au suivant et résumer ainsi tous les livres.

## Le jeu de Mao

Imaginez un jeu où on ne connaît pas les règles, le but du jeu, c'est de gagner, mais aussi de découvrir les règles. Ce jeu simule un système compétitif dans lequel il existerait des règles implicites que l'on apprend par la sanction. Certains dirons que le monde actuel est comme ça, pour moi ce n'est qu'en parti vrai et nous cherchons justement à tout faire pour que le monde ne soit pas comme ça. Le vivre a été très désagréable, incapable de créer du collectif, je me suis retrouvé seul, tout petit et sanctionné.

## Rencontres

De mes rencontres et discussions au café, au dîner ou sur l'herbe au milieu des bernaches, je repars avec encore une tonne de livres à lire. Il y en a un en particulier qui a été au centre de mes échanges : [Le thérapeute et le philosophe de Dany Gerbinet](https://www.librairiesindependantes.com/product/9782356441652/).
