---
date: 2019-03-05
title: "Le pomodoro s'invite le jeudi"
draft: true
authors:
  - quille_julie
vie d'Azaé:
  - interne
outils:
  - pomodoro
description: |
 Chez Azaé nous avons plusieurs approches de l'agilité, la première d'entre elle : l'expérimentation. En tant qu'équipe agile, nous mettons un point d'honneur à améliorer en permanence nos façons de faire pour toujours plus de plaisir à être ensemble. Lors de notre dernière journée commune, consacrée au travail de notre équipe, nous avons expérimenter le pomodoro. Retour sur cette expérience enrichissante.
---

 Chez Azaé nous avons plusieurs approches de l'agilité, la première d'entre elle : l'expérimentation. En tant qu'équipe agile, nous mettons un point d'honneur à améliorer en permanence nos façons de faire pour toujours plus de plaisir à être ensemble. Lors de notre dernière journée commune, consacrée au travail de notre équipe, nous avons expérimenter le pomodoro. Retour sur cette expérience enrichissante.

## Notre processus d'amélioration continue

En tant qu'équipe auto-organisée et apprennante, nous prenons régulièrement le temps de nous interoger sur notre manière de fonctionner. Nous en tirons des expérimentations à tester, nous les mettons en oeuvre et nous faisons un bilan. Le pomodoro du jeudi est issu de cette démarche. En effet, fort de plusieurs années de travail commun, nous cherchons encore comment, tout en étant majoritairement chez nos clients, nous pouvons garder des temps commun pour notre travail d'équipe. Ainsi nous passons plusieurs jours par mois à travailler ensemble. Mais l'organisation de ces moments de partage et d'apprentissages se cherche encore. Lors d'une de nos rétrospective de journée partagée nous avons décidé de tester le pomodoro afin de donner un rythme plus agréable et un engagement plus fort.

## Et le pomodoro : kesako ?

Le [pomodoro](https://fr.wikipedia.org/wiki/Technique_Pomodoro), c'est l'idée d'alterner des temps de travail et des temps de pause afin de favoriser la concentration pendant le travail et de favoriser un travail efficace de par l'utilité des pause. Le fait de s'engager sur une durée courte et determinée a également l'avantage de favoriser l'engagement.

## Notre action en détail

Nous avons choisi :

- une journée de pomodoro (20 min de travail, 5 min de pause)
- un animateur de journée
- un modéle de forum ouvert pour le choix des sujets
- un maximum de 2 sujets en parallèle

Ce à quoi nous nous attendions :

- une plus grande satisfaction vis à vis des sujets traités (sentiment d'utilité de la journée)
- un focus permanent sur les moments de travail
- une conscience accrue de là où nous investissons du temps
- expérimenter une nouvelle façon de faire et en tirer des apprentissages

## Ce qui s'est passé

Notre jeudi pomodoro fut une grande aventure, pleine inattendus ! Tout d'abord notre animateur n'a finalement pas pu être présent, mais ne vous inquiétez pas, cela ne nous a pas décourgaé pour autant.
Ensuite un point sur lequel nous ne nous étions pas mis d'accord était comment choisir les sujets ! En effet, prenons nous les sujets dans l'ordre du backlog ? En fonction des envies de chacun ? Au hasard ? Et surtout ce temps de choix est-il intégré dans les 20 min de travail ? Les 5 min de pause ? Aucun des deux ? Mais dans ce dernier cas, comment faisons-nous pour bénéficier des vertus du timebox, s'il ne s'applique pas aux points sensibles ?
Et bien nous avons expérimenter, un choix de sujets parmis un liste définie ensemble en début de journée, avec discussion jusqu'à concensus sur les sujets sur lesquels nous nous engagions. Enfin nous avons choisi un rythme de 5 min de choix - 20 min de travail sur le sujet - 5 min de pause.

## Bilan de la journée



## Next Step
