---
date: 2018-12-11
title: "Libre Entreprise présent au POSS"
draft: false
authors:
  - quille_julie
vie d'Azaé:
  - nos valeurs
  - Libre-Entreprise

illustration:
    name: poss-2018.png

description: |
  Deux jours de salon et une journée de forum ouvert, le Paris Open Source Summit est l'occasion pour le réseau Libre Entreprise de se retrouver. Faire connaitre nos valeurs et nos pratiques, partager notre passion du libre et de l'oragnisation centrée sur l'humain, ou encore apprendre ensemble, autant de raison de nous retrouver.
---

[Libre Entreprise](http://libre-entreprise.org/) est un réseau d'entreprises, allant de 2 à 17 personnes, qui partagent une passion commune : le logiciel libre et surtout les valeurs qui vont avec. Le [Paris Open Source Summit (POSS)](https://www.opensourcesummit.paris/), est, depuis plusieurs années déjà, l'occasion de partager un stand, mais pas que ! Nous vous racontons notre édition 2018.

## Un réseau : une fierté

Depuis sa création, Azaé appartient au réseau Libre Entreprise (LE). Une volonté forte, affichée depuis sa création, qui reste centrale dans l'ADN d'Azaé. Au-delà d'un acte engagé vis à vis des logiciels libres, c'est avant tout une philosophie d'entreprise, des valeurs et un modèle d'organisation autour desquels les entreprises du réseau LE se retrouvent.
C'est également toute la force du réseau qui est mobilisée dans la démarche : réseau de partage, réseau de réfléxion, réseau de confiance, réseau d'inspiration, la [charte](http://libre-entreprise.org/charte) est claire et le process exigeant. À l'intérieur du réseau, nous savons que ce sont des amis, des frères de valeurs, des gens en qui nous avons confiance.

## Un salon : une passion

Le POSS, en plus d'une occasion de nous retrouver, c'est également le partage de nos savoir faire et de nos savoir être. Ces valeurs auxquelles nous sommes si attachés, et cette organisation, bien connue des communautés libristes, et qui, pour autant, peine à se frayer un chemin dans les organisations extérieures.
Pendant deux jours nous partageons nos expérimentations et nos convictions, nous prônons des organisations saines et humaines, qui incarnent la philosophie du libre jusque dans la manière dont elles se structurent.
C'est également l'occasion de faire le tour des acteurs du libre qui se retrouvent à cet endroit. Un bon moyen de revoir des compagnons de route, mais aussi de se tenir informé des dernières nouveautés et de se familiariser avec les thématiques émergentes dans le milieu.

## After POSS : un forum ouvert

Enfin, étant réparti un peu partout en France, le POSS est aussi l'occasion de nous retrouver sur Paris. Nous profitons du vendredi qui suit pour faire un forum ouvert entre membres libre entreprise.
Nos amis d'[Easter-Eggs](https://www.easter-eggs.com/) nous accueillent et nous chouchoutent.
La journée ne suffira pas pour avoir toutes les discussions que nous souhaitons avoir, mais c'est déjà l'occasion d'une petite remise à jour : l'occasion de reprendre contact et d'avancer sur nos raisons d'être.
Cette année beaucoup de sujets autour du recrutement et des valeurs :
 * comment, où, et qui recruter,
 * comment garantir nos valeurs lorsque l'on grossit,
 * comment diversifier nos entreprises sans nous perdre,
voilà quelques uns des sujets abordés.
La diffusion de nos modèles d'organisation et comment essaimer fait également partie de nos préoccupations. Nous nous y attelons dès maintenant et souhaitons la bienvenue à ceux qui souhaiteraient participer à l'aventure.
