---
date: 2018-10-19
title: "Speed Boat orienté solution"
draft : false
authors:
  - quille_julie
vie d'Azaé:
- publication
outils :
- speedboat
- solution focus
rituels :
- embarquement
description: |
  Il y a quelques semaines, nous vous parlions de la création de notre atelier SpeedBoat-SolutionFocus pour embarquer une équipe et connaitre un peu mieux ses envies et objectifs. Aujourd'hui nous vous informons que nous avons modélisé et publié le format.
illustration:
  name: speedsf2.jpg
---

Il y a quelques semaines, dans l'article [Embarquement d'une nouvelle équipe](https://azae.net/articles/2018-09-14_atelier_solutionfocus_speedboat/) nous vous racontions l'histoire de la naissance d'un atelier, en mixant ce que nous connaissons déjà et nos compétences complémentaires dans l'objectif de répondre au contexte bien particulier dans lequel nous étions. Aujourd'hui nous vous proposons de retrouvé le déroulé dans la description de l'atelier [Speed Boat orienté solution](http://ajiro.fr/games/speedboat_sf/).
