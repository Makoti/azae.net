---
date: 2018-11-06
title: "Thomas nous parle d'Agile Tour Lille"
draft: false
vie d'orateur:
  - Agile Tour Lille
vie d'Azaé:
  - publication
theme:
  - Slow Kata
illustration:
  name: agile-tour-lille-thomas-clavier.png
description: |
 Quelques jours avant d'aller participer à Agile Tour Lille, Thomas nous explique pourquoi il aime cet événement et ce qu'il va y présenter.
---

[Thomas nous raconte Agile Tour Lille 2018](https://youtu.be/xJx0xZi38cI) en vidéo

Coach, formateur, développeur mais aussi orateur, Thomas nous raconte ce qu'il aime à Agile Tour Lille, ainsi que l'atelier Slow Kata qu'il va y présenter.
