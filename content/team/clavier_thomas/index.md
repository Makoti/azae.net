---
nositemap: true
name: Clavier Thomas
id: tclavier
inactive: true
bio: |
  Je suis coach agile chez [Azaé](http://azae.net), enseignant à l'université de Lille et co-fondateur de [Deliverous](http://deliverous.com),
  mes sujets de prédilection sont l'agilité, devops, docker, le lean startup et l'artisanat
  logiciel. Depuis plus de 10 ans, j'essaye de transformer le travail en un jeu, faire progresser
  les geeks et challenger les managers, poser des questions pour faire progresser les équipes.
like: La gastronomie, Vim, le beau code et la course à pied.
---
