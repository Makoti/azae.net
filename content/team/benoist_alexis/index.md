---
nositemap: true
name: Benoist Alexis
id: abenoist
inactive: true
bio: |
  Coach chez [Azaé](http://azae.net) de 2018 à 2020.
  Je suis passionné par la production de logiciels, par leur
  réalisation technique et par les organisations qui les engendrent.
  J'adore transmettre et faire grandir les gens.
like: La piscine, la philo et voyager.
---
