---
nositemap: true
name: Albiez Olivier
id: oalbiez
vcard_name: "Albiez;Olivier"
email: oalbiez@azae.net
mobile: "+33662767812"
organisations:
  - name: Réseau Libre-entreprise
    logo: /assets/images/logo/logo-libre-entreprise.png
    url: https://www.libre-entreprise.org/
social:
  - url: https://gitlab.com/oalbiez
    icon: fa-gitlab
    name: Gitlab
  - url: https://github.com/oalbiez
    icon: fa-github
    name: Github
  - url: https://twitter.com/OlivierAlbiez
    icon: fa-twitter
    name: Twitter
  - url: https://www.linkedin.com/in/olivieralbiez
    icon: fa-linkedin
    name: Linkedin
bio: |
  Je suis artisan logiciel et coach agile chez [Azaé](http://azae.net), passionné des méthodes de travail et de l'auto-organisation dans les entreprises. Curieux par nature, je suis toujours à la recherche de ce qui peut améliorer le travail collectif d'une organisation. Je suis récemment co-fondateur de [Deliverous](http://deliverous.com). Je suis signataire des manifestes [Artisans du Logiciel](http://manifesto.softwarecraftsmanship.org/) et [Agile](http://agilemanifesto.org/).
outputs:
  - html
  - vcard
like: |
  Les discussions politiques, la philosophie, les jeux de plateau et de rôle, le chant.
resources:
  - name: cv.pdf
    src: 'cv.pdf'
---
