Coaching :
  Agile
  Devops
  Lean
  Leadership
  Managment 3.0

Facilitation :
  Serious games
  Lego
  Intelligence collective

Conseil :
  Software craftmanship
  Logiciel libre

Formation :
  Les méthodes agiles (Scrum, Kanban, eXtrem Programming, Lean, etc.)
  Devops
  Software caftmanship (Test Driven Development (TDD), Behavior Driven Development (BDD), refactoring, clean code, etc.)
  Technique (Docker, Java, Go, Puppet, Python)
  L’intelligence collective
  Lean startup
